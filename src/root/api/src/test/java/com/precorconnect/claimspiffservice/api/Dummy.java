package com.precorconnect.claimspiffservice.api;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDtoImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffViewImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDate;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDtoImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementViewImpl;


public class Dummy {

	 /*
    fields
     */
	 private URI uri;

	 private ClaimSpiffId claimSpiffId=new ClaimSpiffIdImpl(10L);

	 private PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(12L);

     private AccountId partnerAccountId  = new AccountIdImpl("001K000001H2Km2IAF");

     private UserId partnerRepUserId= new UserIdImpl("00u5i5aimx8ChKQvB0h7");

     private InstallDate installDate;

     private SellDate sellDate;

     private SpiffAmount spiffAmount = new SpiffAmountImpl(100.00);

     private SpiffClaimedDate spiffClaimedDate= new SpiffClaimedDateImpl(new Timestamp(System.currentTimeMillis()));

     private FacilityName facilityName=new FacilityNameImpl("Precor");

     private InvoiceNumber invoiceNumber=new InvoiceNumberImpl("11111");

     private InvoiceUrl invoiceUrl = new InvoiceUrlImpl("www.testinvoice.com");

     private InvoiceUrl newInvoiceUrl = new InvoiceUrlImpl("http://www.precor.com/test");

 	 private UserId newPartnerRepUserId = new UserIdImpl("00u5i5aimx8ChKQvB0h7");

     private List<ClaimSpiffDto> claimSpiffDtoList = new ArrayList<ClaimSpiffDto>();

     private ClaimSpiffDto claimSpiffDto;

     private ClaimSpiffView claimSpiffView;

     private List<ClaimSpiffView> listClaimSpiffView=new ArrayList<ClaimSpiffView>();

     private OAuth2AccessToken accessToken = null;

    private List<ClaimSpiffId> listClaimSpiffId= new ArrayList<ClaimSpiffId>();

    private SpiffEntitlementDto spiffEntitlementDto;

    private List<SpiffEntitlementDto> spiffEntitlementDtoList = new ArrayList<SpiffEntitlementDto>();

    private SpiffEntitlementView spiffEntitlementView;

	private Collection<SpiffEntitlementView> spiffEntitlementViewList = new ArrayList<SpiffEntitlementView>();

	private SpiffEntitlementId spiffEntitlementId = new SpiffEntitlementIdImpl(1L);

    private List<SpiffEntitlementId> spiffEntitlementIdList = new ArrayList<SpiffEntitlementId>();

    /*
    constructors
     */
    public Dummy() {

		try {
			uri = new URI("http://dev.precorconnect.com");
		} catch (URISyntaxException e) {
			throw new RuntimeException("exception occured in uri creation: ",e);
		}

        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date selledDate;
		try {
			selledDate = formatter.parse("11/21/1995");
		} catch (ParseException e) {
			throw new RuntimeException("Date field parse exception: ",e);
		}

		sellDate = new SellDateImpl(selledDate);

		Date installedDate;
		try {
			installedDate = formatter.parse("11/21/1995");
		} catch (ParseException e) {
			throw new RuntimeException("Date field parse exception: ",e);
		}

		installDate = new InstallDateImpl(installedDate);

		claimSpiffDto = new ClaimSpiffDtoImpl(
				spiffEntitlementId,
        		partnerSaleRegistrationId,
        		partnerAccountId,
        		partnerRepUserId,
        		sellDate,
        		installDate,
        		spiffAmount,
        		spiffClaimedDate,
        		facilityName,
        		invoiceNumber
        		);

		claimSpiffView=new ClaimSpiffViewImpl(
				claimSpiffId,
				partnerSaleRegistrationId,
				partnerAccountId,
				partnerRepUserId,
				installDate,
				spiffAmount,
				spiffClaimedDate,
				facilityName,
				invoiceNumber,
				sellDate
			);

        claimSpiffDtoList.add(claimSpiffDto);
        listClaimSpiffView.add(claimSpiffView);

        listClaimSpiffId.add(claimSpiffId);

        spiffEntitlementDto = new SpiffEntitlementDtoImpl(
        											partnerAccountId,
													partnerSaleRegistrationId,
													facilityName,
													invoiceNumber,
													invoiceUrl,
													partnerRepUserId,
													installDate,
													spiffAmount,
													sellDate
													);
        spiffEntitlementView = new SpiffEntitlementViewImpl(
												spiffEntitlementId,
												partnerAccountId,
												partnerSaleRegistrationId,
												facilityName,
												invoiceNumber,
												invoiceUrl,
												partnerRepUserId,
												installDate,
												spiffAmount,
												sellDate
												);

        spiffEntitlementDtoList.add(spiffEntitlementDto);

        spiffEntitlementIdList.add(spiffEntitlementId);

        spiffEntitlementViewList.add(spiffEntitlementView);
    }

    /*
    getter methods
     */

	public ClaimSpiffId getClaimSpiffId() {
		return claimSpiffId;
	}

	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public AccountId getPartnerAccountId() {
		return partnerAccountId;
	}

	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public InstallDate getInstallDate() {
		return installDate;
	}

	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public FacilityName getFacilityName() {
		return facilityName;
	}

	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	public List<ClaimSpiffDto> getClaimSpiffDtoList() {
		return claimSpiffDtoList;
	}

	public ClaimSpiffDto getClaimSpiffDto() {
		return claimSpiffDto;
	}

	public ClaimSpiffView getClaimSpiffView() {
		return claimSpiffView;
	}

	public List<ClaimSpiffView> getListClaimSpiffView() {
		return listClaimSpiffView;
	}

	public List<ClaimSpiffId> getListClaimSpiffId() {
		return listClaimSpiffId;
	}

	public void setClaimSpiffId(ClaimSpiffId claimSpiffId) {
		this.claimSpiffId = claimSpiffId;
	}

	public void setPartnerSaleRegistrationId(PartnerSaleRegistrationId partnerSaleRegistrationId) {
		this.partnerSaleRegistrationId = partnerSaleRegistrationId;
	}

	public void setPartnerAccountId(AccountId partnerAccountId) {
		this.partnerAccountId = partnerAccountId;
	}

	public void setPartnerRepUserId(UserId partnerRepUserId) {
		this.partnerRepUserId = partnerRepUserId;
	}

	public void setInstallDate(InstallDate installDate) {
		this.installDate = installDate;
	}

	public void setSpiffAmount(SpiffAmount spiffAmount) {
		this.spiffAmount = spiffAmount;
	}

	public void setSpiffClaimedDate(SpiffClaimedDate spiffClaimedDate) {
		this.spiffClaimedDate = spiffClaimedDate;
	}


	public void setFacilityName(FacilityName facilityName) {
		this.facilityName = facilityName;
	}

	public void setInvoiceNumber(InvoiceNumber invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public void setClaimSpiffDtoList(List<ClaimSpiffDto> claimSpiffDtoList) {
		this.claimSpiffDtoList = claimSpiffDtoList;
	}

	public void setClaimSpiffDto(ClaimSpiffDto claimSpiffDto) {
		this.claimSpiffDto = claimSpiffDto;
	}

	public void setClaimSpiffView(ClaimSpiffView claimSpiffView) {
		this.claimSpiffView = claimSpiffView;
	}

	public void setListClaimSpiffView(List<ClaimSpiffView> listClaimSpiffView) {
		this.listClaimSpiffView = listClaimSpiffView;
	}

	public void setListClaimSpiffId(List<ClaimSpiffId> listClaimSpiffId) {
		this.listClaimSpiffId = listClaimSpiffId;
	}

	public OAuth2AccessToken getAccessToken() {
		return accessToken;
	}

	public URI getUri() {
		return uri;
	}

	public InvoiceUrl getInvoiceUrl() {
		return invoiceUrl;
	}

	public SpiffEntitlementDto getSpiffEntitlementDto() {
		return spiffEntitlementDto;
	}

	public List<SpiffEntitlementDto> getSpiffEntitlementDtoList() {
		return spiffEntitlementDtoList;
	}

	public SpiffEntitlementView getSpiffEntitlementView() {
		return spiffEntitlementView;
	}

	public Collection<SpiffEntitlementView> getSpiffEntitlementViewList() {
		return spiffEntitlementViewList;
	}

	public SpiffEntitlementId getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	public List<SpiffEntitlementId> getSpiffEntitlementIdList() {
		return spiffEntitlementIdList;
	}

	public InvoiceUrl getNewInvoiceUrl() {
		return newInvoiceUrl;
	}

	public UserId getNewPartnerRepUserId() {
		return newPartnerRepUserId;
	}

}
