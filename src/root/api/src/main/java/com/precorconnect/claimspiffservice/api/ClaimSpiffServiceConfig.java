package com.precorconnect.claimspiffservice.api;

import com.precorconnect.claimspiffservice.database.DatabaseAdapterConfig;
import com.precorconnect.claimspiffservice.identity.IdentityServiceAdapterConfig;


public interface ClaimSpiffServiceConfig {

    DatabaseAdapterConfig getDatabaseAdapterConfig();
    
    IdentityServiceAdapterConfig getIdentityServiceAdapterConfig();

}
