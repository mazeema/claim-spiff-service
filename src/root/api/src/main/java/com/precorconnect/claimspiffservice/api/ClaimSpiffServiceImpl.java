package com.precorconnect.claimspiffservice.api;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.core.Core;
import com.precorconnect.claimspiffservice.core.CoreImpl;
import com.precorconnect.claimspiffservice.database.DatabaseAdapterImpl;
import com.precorconnect.claimspiffservice.identity.IdentityServiceAdapterImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;


public class ClaimSpiffServiceImpl
		implements ClaimSpiffService {

    /*
    fields
     */
    private final Core core;

    /*
    constructors
     */
    public ClaimSpiffServiceImpl(
            @NonNull ClaimSpiffServiceConfig config
    ) {

        core =
                new CoreImpl(
                        new DatabaseAdapterImpl(
                                config.getDatabaseAdapterConfig()
                        ),
                        new IdentityServiceAdapterImpl(
                                config.getIdentityServiceAdapterConfig()
                        )
                );

    }

	@Override
	public Collection<ClaimSpiffId> addCliamSpiffs(
			Collection<ClaimSpiffDto> claimSpiffDto,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException{

		return
				core
				.addClaimSpiffs(
						claimSpiffDto,
						accessToken
						);

	}

	@Override
	public Collection<ClaimSpiffView> getClaimSpiffsWithIds(
			@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException {
		return
				core
				.getClaimSpiffsWithIds(
						listClaimSpiffsIds,
						accessToken
						);
	}

	@Override
	public Collection<SpiffEntitlementId> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements,
			@NonNull OAuth2AccessToken accessToken
		    ) throws AuthenticationException, AuthorizationException {

		return
				core
				.createSpiffEntitlements(
						spiffEntitlements,
						accessToken
				 );
	}

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken
		    ) throws AuthenticationException, AuthorizationException {

		return
				core
				.listEntitlementsWithPartnerId(
						accountId,
						accessToken
				 );
	}
	
	@Override
	public Collection<SpiffLogView> getEntitlementWithPartnerSaleRegistrationId(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId, 
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
		
		return
				core
					.getEntitlementWithPartnerSaleRegistrationId(
							partnerSaleRegistrationId,
							accessToken
							);
	}

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken
		    ) throws AuthenticationException, AuthorizationException {

				core
					.updateInvoiceUrl(
							partnerSaleRegistrationId,
							invoiceUrl,
							accessToken
							);

	}

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserid,
			@NonNull OAuth2AccessToken accessToken
		    ) throws AuthenticationException, AuthorizationException {

		core
			.updatePartnerRep(
					partnerSaleRegistrationId,
					partnerRepUserid,
					accessToken
					);

	}

	@Override
	public Collection<SpiffLogView> listSpiffLogs(
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {

		return
				core
					.listSpiffLogs(
							accessToken
							);
	}

	@Override
	public Collection<SpiffLogView> listSpiffLogsWithId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {

		return
				core
					.listSpiffLogsWithId(
							accountId,
							accessToken
							);
	}

	@Override
	public void updateSpiffLog(
			@NonNull SpiffLogDto spiffLogDto,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {

		core
			.updateSpiffLog(
					spiffLogDto,
					accessToken
				);

	}

}
