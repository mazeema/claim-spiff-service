package com.precorconnect.claimspiffservice.api;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.database.DatabaseAdapterConfig;
import com.precorconnect.claimspiffservice.identity.IdentityServiceAdapterConfig;


public class ClaimSpiffServiceConfigImpl 
		implements ClaimSpiffServiceConfig {

    /*
    fields
     */
    private final DatabaseAdapterConfig databaseAdapterConfig;
    
    private final IdentityServiceAdapterConfig identityServiceAdapterConfig;


    /*
    constructors
     */
    public ClaimSpiffServiceConfigImpl(
            @NonNull final DatabaseAdapterConfig databaseAdapterConfig,
            @NonNull final IdentityServiceAdapterConfig identityServiceAdapterConfig
    ) {

    	this.databaseAdapterConfig =
                guardThat(
                        "databaseAdapterConfig",
                         databaseAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceAdapterConfig =
                guardThat(
                        "identityServiceAdapterConfig",
                        identityServiceAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();
    }

    /*
    getter methods
    */
    @Override
    public DatabaseAdapterConfig getDatabaseAdapterConfig() {
        return databaseAdapterConfig;
    }

    @Override
    public IdentityServiceAdapterConfig getIdentityServiceAdapterConfig() {
        return identityServiceAdapterConfig;
    }
    
}
