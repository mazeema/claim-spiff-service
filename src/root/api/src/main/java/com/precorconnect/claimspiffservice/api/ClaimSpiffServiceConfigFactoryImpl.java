package com.precorconnect.claimspiffservice.api;

import com.precorconnect.claimspiffservice.database.DatabaseAdapterConfigFactoryImpl;
import com.precorconnect.claimspiffservice.identity.IdentityServiceAdapterConfigFactoryImpl;

public class ClaimSpiffServiceConfigFactoryImpl implements
		ClaimSpiffServiceConfigFactory {

	@Override
	public ClaimSpiffServiceConfig construct() {
		
		return 
				new ClaimSpiffServiceConfigImpl(
						new DatabaseAdapterConfigFactoryImpl()
												  .construct(),
						new IdentityServiceAdapterConfigFactoryImpl()
													.construct()
					);
		
				
	}

}
