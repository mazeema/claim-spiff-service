package com.precorconnect.claimspiffservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;


public class ClaimSpiffWebDto {

	/*
    fields
     */
	private final Long spiffEntitlementId;

    private final Long partnerSaleRegistrationId;

    private final String partnerAccountId;

    private final String partnerRepUserId;

    private final String sellDate;

    private final String installDate;

    private final Double spiffAmount;

    private final String spiffClaimedDate;

    private final String facilityName;

    private final String invoiceNumber;


    /*
    constructors
     */
    public ClaimSpiffWebDto()
     {
    	spiffEntitlementId = 0L;

    	partnerSaleRegistrationId = 0L;

    	partnerAccountId = null;

    	partnerRepUserId = null;

    	sellDate = null;

    	installDate = null;

    	spiffAmount = 0d;

    	spiffClaimedDate = null;

    	facilityName = null;

    	invoiceNumber = null;


    }

    public ClaimSpiffWebDto(
    		@NonNull final Long spiffEntitlementId,
    		@NonNull final Long partnerSaleRegistrationId,
    		@NonNull final String partnerAccountId,
    		@NonNull final String partnerRepUserId,
    		@NonNull final String sellDate,
    		@Nullable final String installDate,
    		@NonNull final Double spiffAmount,
    		@Nullable final String spiffClaimedDate,
    		@NonNull final String facilityName,
            @NonNull final String invoiceNumber
    ) {

    	this.spiffEntitlementId =
                guardThat(
                        "spiffEntitlementId",
                        spiffEntitlementId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                         partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                         partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                         partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

    	this.installDate = installDate;
              

    	this.spiffAmount =
                guardThat(
                        "spiffAmount",
                         spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffClaimedDate = spiffClaimedDate;


    	this.facilityName =
                guardThat(
                        "facilityName",
                         facilityName
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                         invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
    */
    public Long getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

    public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getPartnerAccountId() {
		return partnerAccountId;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public String getSellDate() {
		return sellDate;
	}

	public Optional<String> getInstallDate() {
		return Optional.ofNullable(installDate);
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public String getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

}
