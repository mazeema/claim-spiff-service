package com.precorconnect.claimspiffservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;


public class ClaimSpiffWebView {

	/*
    fields
     */
	private final Long claimId;

    private final Long partnerSaleRegistrationId;

    private final String partnerAccountId;

    private final String partnerRepUserId;

    private final String installDate;

    private final Double spiffAmount;

    private final String spiffClaimedDate;

    private final String facilityName;

    private final String invoiceNumber;

    private final String sellDate;


    /*constructors*/

    public ClaimSpiffWebView(){

    	claimId = 0L;

        partnerSaleRegistrationId = 0L;

        partnerAccountId = null;

        partnerRepUserId = null;

        installDate = null;

        spiffAmount = 0d;

        spiffClaimedDate = null;

        facilityName = null;

        invoiceNumber = null;

        sellDate = null;

    }

	public ClaimSpiffWebView(
			@NonNull Long claimId,
			@NonNull Long partnerSaleRegistrationId,
			@NonNull String partnerAccountId,
			@NonNull String partnerRepUserId,
			@Nullable String installDate,
			@NonNull Double spiffAmount,
			@NonNull String spiffClaimedDate,
			@NonNull String facilityName,
			@NonNull String invoiceNumber,
			@NonNull String sellDate
			) {

		this.claimId =
                guardThat(
                        "claimId",
                         claimId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                         partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                         partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                         partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

		this.installDate = installDate;
                

		this.spiffAmount =
                guardThat(
                        "spiffAmount",
                         spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffClaimedDate =
                guardThat(
                        "spiffClaimedDate",
                         spiffClaimedDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.facilityName =
                guardThat(
                        "facilityName",
                         facilityName
                )
                        .isNotNull()
                        .thenGetValue();


		this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                         invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

		this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

	}

	/*getter methods*/

	public Long getClaimId() {
		return claimId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getPartnerAccountId() {
		return partnerAccountId;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public String getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getSellDate() {
		return sellDate;
	}


}
