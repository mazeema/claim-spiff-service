package com.precorconnect.claimspiffservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class SpiffEntitlementWebView {

	/*
    fields
     */
	private final Long spiffEntitlementId;

    private final String accountId;

    private final Long partnerSaleRegistrationId;

    private final String facilityName;

    private final String invoiceNumber;

    private final String invoiceUrl;

    private final String partnerRepUserId;

    private final String installDate;

    private final Double spiffAmount;

    private final String sellDate;

    /*
    constructors
     */
    public SpiffEntitlementWebView(){

    	spiffEntitlementId = 0L;
    	accountId = null;
    	facilityName = null;
    	installDate = null;
    	invoiceNumber = null;
    	invoiceUrl = null;
    	partnerRepUserId = null;
    	partnerSaleRegistrationId = 0L;
    	spiffAmount = 0.0;
    	sellDate = null;

    }

    public SpiffEntitlementWebView(
    		@NonNull final Long spiffEntitlementId,
    		@NonNull final String accountId,
            @NonNull final Long partnerSaleRegistrationId,
            @NonNull final String facilityName,
            @NonNull final String invoiceNumber,
            @NonNull final String invoiceUrl,
            final String partnerRepUserId,
            @Nullable final String installDate,
            @NonNull final Double spiffAmount,
            @NonNull final String sellDate
    ) {

    	this.spiffEntitlementId =
                guardThat(
                        "spiffEntitlementId",
                        spiffEntitlementId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.accountId =
                guardThat(
                        "accountId",
                        accountId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();
    	this.facilityName =
                guardThat(
                        "facilityName",
                        facilityName
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                        invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceUrl = invoiceUrl;

    	this.partnerRepUserId = partnerRepUserId;

    	this.installDate = installDate;
              
    	this.spiffAmount =
                guardThat(
                        "spiffAmount",
                        spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

    	this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
    */
    public Long getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	public String getAccountId() {
		return accountId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public String getSellDate() {
		return sellDate;
	}


}
