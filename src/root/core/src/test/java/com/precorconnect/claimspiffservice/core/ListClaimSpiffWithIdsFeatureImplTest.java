package com.precorconnect.claimspiffservice.core;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.precorconnect.AuthenticationException;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;



@RunWith(MockitoJUnitRunner.class)
public class ListClaimSpiffWithIdsFeatureImplTest {

	/*
    fields
     */
	@Mock
	private DatabaseAdapter databaseAdapter;

	@InjectMocks
	private ListClaimSpiffWithIdsFeatureImpl listClaimSpiffWithIdsFeatureImpl;

	private Dummy dummy = new Dummy();



	@Test
	public void testGetClaimSpiffsWithIds_whenClaimIds_shouldReturnClaimSpiffs() throws AuthenticationException{

		when(databaseAdapter
				.getClaimSpiffsWithIds(
						dummy.getListClaimSpiffId()
						)
			)
			.thenReturn(
				dummy.getListClaimSpiffView()
						);

		Collection<ClaimSpiffView> claimSpiffViewsList =
					listClaimSpiffWithIdsFeatureImpl
						.getClaimSpiffsWithIds(
								dummy.getListClaimSpiffId()
					);


		/*assert*/

		assertThat(claimSpiffViewsList.size())
		.isGreaterThan(0);

	}
}
