package com.precorconnect.claimspiffservice.core;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;



@RunWith(MockitoJUnitRunner.class)
public class AddClaimSpiffFeatureImplTest {

	/*
    fields
     */
	@Mock
	private DatabaseAdapter databaseAdapter;

	@InjectMocks
	private AddClaimSpiffFeatureImpl addClaimSpiffFeatureImpl;

	private Dummy dummy = new Dummy();



	@Test
	public void testAddCliamSpiffs_whenClaimSpiffDtos_shouldReturnIds() throws AuthenticationException{

		when(databaseAdapter
				.addClaimSpiffs(
						dummy.getClaimSpiffDtoList()
						)
			)
			.thenReturn(
						dummy.getListClaimSpiffId()
						);

		Collection<ClaimSpiffId> claimSpiffIdList = addClaimSpiffFeatureImpl.addClaimSpiffs(
				dummy.getClaimSpiffDtoList()
			);

		assertEquals(
				dummy.getListClaimSpiffId().size(),
				claimSpiffIdList.size()
				);

	}




    @SuppressWarnings("unchecked")
	@Test(expected=AuthenticationException.class)
    public void testAddCliamSpiffs_whenAuthenticationFailed_shouldThrowAuthenticationException() throws AuthenticationException{

    	/*arrange*/

    	IdentityServiceAdapter identityServiceAdapter = mock(IdentityServiceAdapter.class);

    	when(identityServiceAdapter.getPartnerRepAccessContext(any(OAuth2AccessToken.class)))
        .thenThrow(AuthenticationException.class);

    	 Core objectUnderTest =
                 new CoreImpl(
                         mock(DatabaseAdapter.class),
                         identityServiceAdapter
                 );

    	 /*act*/

    	 objectUnderTest.addClaimSpiffs(dummy.getClaimSpiffDtoList(), any(OAuth2AccessToken.class));

    }
}
