package com.precorconnect.claimspiffservice.core;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public interface Core {


    Collection<ClaimSpiffId> addClaimSpiffs(
    		Collection<ClaimSpiffDto> listClaimSpiffs,
    		 @NonNull OAuth2AccessToken accessToken
    	    ) throws AuthenticationException;

    Collection<ClaimSpiffView> getClaimSpiffsWithIds(
    		@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds,
    		@NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException;

	 Collection<SpiffEntitlementId> createSpiffEntitlements(
				@NonNull List<SpiffEntitlementDto> spiffEntitlementDtos,
				@NonNull OAuth2AccessToken accessToken
				) throws AuthenticationException, AuthorizationException;
	
	 Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
							@NonNull AccountId accountId,
							@NonNull OAuth2AccessToken accessToken
						) throws AuthenticationException, AuthorizationException;
	
	 Collection<SpiffLogView> getEntitlementWithPartnerSaleRegistrationId(
				@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
				@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException;
	 
	 void updateInvoiceUrl(
			 	@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 	@NonNull InvoiceUrl invoiceUrl,
			 	@NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException, AuthorizationException;
	
	 void updatePartnerRep(
			 	@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 	@NonNull UserId partnerRepUserId,
			 	@NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException, AuthorizationException;
	
	 Collection<SpiffLogView> listSpiffLogs(
			 @NonNull OAuth2AccessToken accessToken
				) throws AuthenticationException, AuthorizationException;
	
	 Collection<SpiffLogView> listSpiffLogsWithId(
			 @NonNull AccountId accountId,
			 @NonNull OAuth2AccessToken accessToken
				) throws AuthenticationException, AuthorizationException;
	
	 void updateSpiffLog(
			 @NonNull SpiffLogDto spiffLogDto,
			 @NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException;


}
