package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;

@Singleton
public class UpdateSpiffLogFeatureImpl implements
		UpdateSpiffLogFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public UpdateSpiffLogFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public void updateSpiffLog(
			@NonNull SpiffLogDto spiffLogDto
			) throws AuthenticationException,AuthorizationException {

		databaseAdapter
				.updateSpiffLog(
								spiffLogDto
								);

	}

}
