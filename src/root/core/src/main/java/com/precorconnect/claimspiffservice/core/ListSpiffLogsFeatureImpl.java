package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

@Singleton
public class ListSpiffLogsFeatureImpl implements ListSpiffLogsFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public ListSpiffLogsFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<SpiffLogView> listSpiffLogs()
			throws AuthenticationException {

		return
				databaseAdapter
					.listSpiffLogs();

	}

}
