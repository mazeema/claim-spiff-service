package com.precorconnect.claimspiffservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public interface GetSpiffEntitlementFeature {

	Collection<SpiffLogView> getEntitlementWithPartnerSaleRegistrationId(
			@NonNull PartnerSaleRegistrationId partnerSaleRegId
			) throws AuthenticationException, AuthorizationException;
}
