package com.precorconnect.claimspiffservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public interface ListSpiffLogsWithIdFeature {

	Collection<SpiffLogView> listSpiffLogsWithId(
			@NonNull AccountId accountId
			) throws AuthenticationException;

}
