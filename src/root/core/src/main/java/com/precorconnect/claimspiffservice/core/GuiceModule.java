package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;



class GuiceModule extends
        AbstractModule {

    private final DatabaseAdapter databaseAdapter;

    private final IdentityServiceAdapter identityServiceAdapter;

    public GuiceModule(
            @NonNull final DatabaseAdapter databaseAdapter,
            @NonNull final IdentityServiceAdapter identityServiceAdapter
            ){
    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceAdapter =
                guardThat(
                        "identityServiceAdapter",
                         identityServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindAdapters();
        bindFeatures();

    }

    private void bindAdapters() {

        bind(DatabaseAdapter.class)
                .toInstance(databaseAdapter);

        bind(IdentityServiceAdapter.class)
        		.toInstance(identityServiceAdapter);

    }

    private void bindFeatures() {

        bind(ListClaimSpiffWithIdsFeature.class)
        		.to(ListClaimSpiffWithIdsFeatureImpl.class);

        bind(AddClaimSpiffFeature.class)
				.to(AddClaimSpiffFeatureImpl.class);

        bind(ListSpiffEntitlementFeature.class)
        		.to(ListSpiffEntitlementFeatureImpl.class);

        bind(GetSpiffEntitlementFeature.class)
        	.to(GetSpiffEntitlementFeatureImpl.class);

        bind(CreateSpiffEntitlementFeature.class)
				.to(CreateSpiffEntitlementFeatureImpl.class);

        bind(UpdatePartnerRepFeature.class)
				.to(UpdatePartnerRepFeatureImpl.class);

        bind(UpdateInvoiceUrlFeature.class)
				.to(UpdateInvoiceUrlFeatureImpl.class);

		bind(ListSpiffLogsFeature.class)
				.to(ListSpiffLogsFeatureImpl.class);

		bind(ListSpiffLogsWithIdFeature.class)
				.to(ListSpiffLogsWithIdFeatureImpl.class);

		bind(UpdateSpiffLogFeature.class)
				.to(UpdateSpiffLogFeatureImpl.class);

    }
}
