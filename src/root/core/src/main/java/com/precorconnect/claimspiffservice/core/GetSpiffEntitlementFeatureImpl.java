package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

@Singleton
class GetSpiffEntitlementFeatureImpl 
		implements GetSpiffEntitlementFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public GetSpiffEntitlementFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }


	@Override
	public Collection<SpiffLogView> getEntitlementWithPartnerSaleRegistrationId(
				@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId
			) throws AuthenticationException, AuthorizationException{


		return
                databaseAdapter
                	.getEntitlementWithPartnerSaleRegistrationId(partnerSaleRegistrationId);
	}


}

