package com.precorconnect.claimspiffservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;

public interface UpdateSpiffLogFeature {

	void updateSpiffLog(
			@NonNull SpiffLogDto spiffLogDto
			) throws AuthenticationException, AuthorizationException;

}
