package com.precorconnect.claimspiffservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;

public interface UpdateInvoiceUrlFeature {

	 void updateInvoiceUrl(
			 @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 @NonNull InvoiceUrl invoiceUrl
			 ) throws AuthenticationException, AuthorizationException;

}
