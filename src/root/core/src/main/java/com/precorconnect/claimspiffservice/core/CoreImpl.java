package com.precorconnect.claimspiffservice.core;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.EmployeeAccessContext;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedBy;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedByImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDtoImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;


public class CoreImpl implements
        Core {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public CoreImpl(
            @NonNull DatabaseAdapter databaseAdapter,
            @NonNull IdentityServiceAdapter identityServiceAdapter
    ) {

    	GuiceModule guiceModule =
                new GuiceModule(
                        databaseAdapter,
                        identityServiceAdapter
                        );


        injector = Guice.createInjector(guiceModule);
    }

    @Override
	public Collection<ClaimSpiffId> addClaimSpiffs(
			Collection<ClaimSpiffDto> listClaimSpiffs,
			@NonNull OAuth2AccessToken accessToken
		    ) throws AuthenticationException {

	      injector
	      		.getInstance(IdentityServiceAdapter.class)
	            .getPartnerRepAccessContext(accessToken);

		 return
				 injector
				 	.getInstance(AddClaimSpiffFeature.class)
				 	.addClaimSpiffs(
						 listClaimSpiffs
						 );
	}

	@Override
	public Collection<ClaimSpiffView> getClaimSpiffsWithIds(
			@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException {

	      injector
    			.getInstance(IdentityServiceAdapter.class)
    			.getPartnerRepAccessContext(accessToken);

		return
				injector
		         	.getInstance(ListClaimSpiffWithIdsFeature.class)
		         	.getClaimSpiffsWithIds(
		         			listClaimSpiffsIds
		         			);
	}

	@Override
	public Collection<SpiffEntitlementId> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlementDtos,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

		injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

		return
                injector
                        .getInstance(CreateSpiffEntitlementFeature.class)
                        	.createSpiffEntitlements(
                        			spiffEntitlementDtos
                        			);
	}

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

		injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

		return
                injector
                        .getInstance(ListSpiffEntitlementFeature.class)
                        	.listEntitlementsWithPartnerId(
                        			accountId
                        			);
	}
	
	@Override
	public Collection<SpiffLogView> getEntitlementWithPartnerSaleRegistrationId(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
		
		injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);
		
		return
                injector
                	.getInstance(GetSpiffEntitlementFeature.class)
                	.getEntitlementWithPartnerSaleRegistrationId(
                			partnerSaleRegistrationId
                        			);
	}

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

    	injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

                injector
                        .getInstance(UpdateInvoiceUrlFeature.class)
                        	.updateInvoiceUrl(
                        			partnerSaleRegistrationId,
                        			invoiceUrl
                        			);
	}

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserId,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

    	injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

                injector
                        .getInstance(UpdatePartnerRepFeature.class)
                        	.updatePartnerRep(
                        			partnerSaleRegistrationId,
                        			partnerRepUserId
                        			);


	}

	@Override
	public Collection<SpiffLogView> listSpiffLogs(
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

		injector
			.getInstance(IdentityServiceAdapter.class)
			.getEmployeeAccessContext(accessToken);

		return
				injector
					.getInstance(ListSpiffLogsFeature.class)
					.listSpiffLogs();

	}

	@Override
	public Collection<SpiffLogView> listSpiffLogsWithId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {

		injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

		return
				injector
					.getInstance(ListSpiffLogsWithIdFeature.class)
					.listSpiffLogsWithId(accountId);

	}

	@Override
	public void updateSpiffLog(
			 @NonNull SpiffLogDto spiffLogDto,
			 @NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException,AuthorizationException {

		EmployeeAccessContext employeeAccessContext =injector
													.getInstance(IdentityServiceAdapter.class)
													.getEmployeeAccessContext(accessToken);

		ReviewedBy reviewedBy = new ReviewedByImpl(employeeAccessContext.getFirstName().getValue());

		SpiffLogDto modifiedSpiffLogDto =
								new SpiffLogDtoImpl(
												spiffLogDto.getClaimSpiffId(),
												spiffLogDto.getPartnerSaleRegistrationId(),
												spiffLogDto.getPartnerAccountId(),
												spiffLogDto.getPartnerRepUserId(),
												spiffLogDto.getSellDate(),
												spiffLogDto.getInstallDate(),
												spiffLogDto.getSpiffAmount(),
												spiffLogDto.getSpiffClaimedDate(),
												spiffLogDto.getFacilityName(),
												spiffLogDto.getInvoiceNumber(),
												spiffLogDto.getReviewStatus(),
												reviewedBy,
												spiffLogDto.getReviewedDate()
												);
		injector
				.getInstance(UpdateSpiffLogFeature.class)
				.updateSpiffLog(
								modifiedSpiffLogDto
								);

	}

}
