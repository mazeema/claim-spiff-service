package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public class ListSpiffLogsWithIdFeatureImpl implements
		ListSpiffLogsWithIdFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public ListSpiffLogsWithIdFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<SpiffLogView> listSpiffLogsWithId(
			@NonNull AccountId accountId
			) throws AuthenticationException {

		return
				databaseAdapter
					.listSpiffLogsWithId(accountId);
	}

}
