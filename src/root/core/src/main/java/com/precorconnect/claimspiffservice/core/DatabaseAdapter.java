package com.precorconnect.claimspiffservice.core;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;


public interface DatabaseAdapter {


	Collection<ClaimSpiffId> addClaimSpiffs(
    		Collection<ClaimSpiffDto> listClaimSpiffs
    	    ) ;

    Collection<ClaimSpiffView> getClaimSpiffsWithIds(
    		Collection<ClaimSpiffId> listClaimSpiffsIds
    		);

    Collection<SpiffEntitlementId> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlementDtos
			);

    Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
						@NonNull AccountId accountId
						);

    Collection<SpiffLogView> getEntitlementWithPartnerSaleRegistrationId(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId
			);

    void updateInvoiceUrl(
		 	@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
		 	@NonNull InvoiceUrl invoiceUrl
		 	);

    void updatePartnerRep(
		 	@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
		 	@NonNull UserId delaerRepUserid
		 	);

    Collection<SpiffLogView> listSpiffLogs();

    Collection<SpiffLogView> listSpiffLogsWithId(
    							@NonNull AccountId accountId
    							);

    public void updateSpiffLog(
    		@NonNull SpiffLogDto spiffLogDto
			);

}
