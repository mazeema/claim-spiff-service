package com.precorconnect.claimspiffservice.database;

public final class ConfigFactory {
	
	public Config construct(){
		
		return
				new Config(
					new DatabaseAdapterConfigFactoryImpl()
														 .construct()
				);
	}

}
