package com.precorconnect.claimspiffservice.database;



import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spifflog")
class ClaimSpiff {

    /*
    fields
     */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long claimId;

    private Long partnerSaleRegistrationId;

    private String partnerAccountId;

    private String partnerRepUserId;

    private Date installDate;

    private Double spiffAmount;

    private Timestamp spiffClaimedDate;

    private String facilityName;

    private String invoiceNumber;

    private Date sellDate;

    private String reviewStatus;

    private String reviewedBy;

    private Date reviewedDate;

    /*
    getter & setter methods
    */
    public Long getClaimId() {
		return claimId;
	}

	public void setClaimId(Long claimId) {
		this.claimId = claimId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public void setPartnerSaleRegistrationId(Long partnerSaleRegistrationId) {
		this.partnerSaleRegistrationId = partnerSaleRegistrationId;
	}

	public String getPartnerAccountId() {
		return partnerAccountId;
	}

	public void setPartnerAccountId(String partnerAccountId) {
		this.partnerAccountId = partnerAccountId;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public void setPartnerRepUserId(String partnerRepUserId) {
		this.partnerRepUserId = partnerRepUserId;
	}

	public Date getInstallDate() {
		return installDate;
	}

	public void setInstallDate(Date installDate) {
		this.installDate = installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public void setSpiffAmount(Double spiffAmount) {
		this.spiffAmount = spiffAmount;
	}

	public Timestamp getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public void setSpiffClaimedDate(Timestamp spiffClaimedDate) {
		this.spiffClaimedDate = spiffClaimedDate;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}

	public String getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public String getReviewedBy() {
		return reviewedBy;
	}

	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	public Date getReviewedDate() {
		return reviewedDate;
	}

	public void setReviewedDate(Date reviewedDate) {
		this.reviewedDate = reviewedDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((claimId == null) ? 0 : claimId.hashCode());
		result = prime * result
				+ ((facilityName == null) ? 0 : facilityName.hashCode());
		result = prime * result
				+ ((installDate == null) ? 0 : installDate.hashCode());
		result = prime * result
				+ ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime
				* result
				+ ((partnerAccountId == null) ? 0 : partnerAccountId.hashCode());
		result = prime
				* result
				+ ((partnerRepUserId == null) ? 0 : partnerRepUserId.hashCode());
		result = prime
				* result
				+ ((partnerSaleRegistrationId == null) ? 0
						: partnerSaleRegistrationId.hashCode());
		result = prime * result
				+ ((reviewStatus == null) ? 0 : reviewStatus.hashCode());
		result = prime * result
				+ ((reviewedBy == null) ? 0 : reviewedBy.hashCode());
		result = prime * result
				+ ((reviewedDate == null) ? 0 : reviewedDate.hashCode());
		result = prime * result
				+ ((sellDate == null) ? 0 : sellDate.hashCode());
		result = prime * result
				+ ((spiffAmount == null) ? 0 : spiffAmount.hashCode());
		result = prime
				* result
				+ ((spiffClaimedDate == null) ? 0 : spiffClaimedDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ClaimSpiff other = (ClaimSpiff) obj;
		if (claimId == null) {
			if (other.claimId != null) {
				return false;
			}
		} else if (!claimId.equals(other.claimId)) {
			return false;
		}
		if (facilityName == null) {
			if (other.facilityName != null) {
				return false;
			}
		} else if (!facilityName.equals(other.facilityName)) {
			return false;
		}
		if (installDate == null) {
			if (other.installDate != null) {
				return false;
			}
		} else if (!installDate.equals(other.installDate)) {
			return false;
		}
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null) {
				return false;
			}
		} else if (!invoiceNumber.equals(other.invoiceNumber)) {
			return false;
		}
		if (partnerAccountId == null) {
			if (other.partnerAccountId != null) {
				return false;
			}
		} else if (!partnerAccountId.equals(other.partnerAccountId)) {
			return false;
		}
		if (partnerRepUserId == null) {
			if (other.partnerRepUserId != null) {
				return false;
			}
		} else if (!partnerRepUserId.equals(other.partnerRepUserId)) {
			return false;
		}
		if (partnerSaleRegistrationId == null) {
			if (other.partnerSaleRegistrationId != null) {
				return false;
			}
		} else if (!partnerSaleRegistrationId
				.equals(other.partnerSaleRegistrationId)) {
			return false;
		}
		if (reviewStatus == null) {
			if (other.reviewStatus != null) {
				return false;
			}
		} else if (!reviewStatus.equals(other.reviewStatus)) {
			return false;
		}
		if (reviewedBy == null) {
			if (other.reviewedBy != null) {
				return false;
			}
		} else if (!reviewedBy.equals(other.reviewedBy)) {
			return false;
		}
		if (reviewedDate == null) {
			if (other.reviewedDate != null) {
				return false;
			}
		} else if (!reviewedDate.equals(other.reviewedDate)) {
			return false;
		}
		if (sellDate == null) {
			if (other.sellDate != null) {
				return false;
			}
		} else if (!sellDate.equals(other.sellDate)) {
			return false;
		}
		if (spiffAmount == null) {
			if (other.spiffAmount != null) {
				return false;
			}
		} else if (!spiffAmount.equals(other.spiffAmount)) {
			return false;
		}
		if (spiffClaimedDate == null) {
			if (other.spiffClaimedDate != null) {
				return false;
			}
		} else if (!spiffClaimedDate.equals(other.spiffClaimedDate)) {
			return false;
		}
		return true;
	}

}
