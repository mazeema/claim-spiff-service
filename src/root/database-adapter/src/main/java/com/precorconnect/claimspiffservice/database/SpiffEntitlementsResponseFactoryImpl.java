package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDate;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementViewImpl;

@Singleton
public class SpiffEntitlementsResponseFactoryImpl implements SpiffEntitlementsResponseFactory {

	@Override
	public SpiffEntitlementView construct(
									SpiffEntitlement spiffEntitlement){

		guardThat(
                "spiffEntitlement",
                spiffEntitlement
              )
               .isNotNull();

		SpiffEntitlementId spiffEntitlementId=
				new SpiffEntitlementIdImpl(
						spiffEntitlement
						.getSpiffEntitlementId()
						);

		AccountId accountId=
				new AccountIdImpl(
						spiffEntitlement
						.getAccountId()
						);

		PartnerSaleRegistrationId partnerSaleRegistrationId=
		  new PartnerSaleRegistrationIdImpl(
				  spiffEntitlement
				  .getPartnerSaleRegistrationId()
				  );

		FacilityName facilityName=
				new FacilityNameImpl(
						spiffEntitlement
						.getFacilityName()
						);

		InvoiceNumber invoiceNumber=
				new InvoiceNumberImpl(
						spiffEntitlement
						.getInvoiceNumber()
				);

		InvoiceUrl invoiceUrl=
				new InvoiceUrlImpl(
						spiffEntitlement
						.getInvoiceUrl()
				);

		UserId partnerRepUserId = null;

		if (spiffEntitlement.getPartnerRepUserId() != null
				&& !spiffEntitlement.getPartnerRepUserId().isEmpty()) {
			partnerRepUserId = new UserIdImpl(spiffEntitlement
					.getPartnerRepUserId().trim());
		}
		InstallDate installDate = null;
		
		if(spiffEntitlement
						.getInstallDate()!=null) {
		installDate=
				new InstallDateImpl(
						spiffEntitlement
						.getInstallDate()
				);
		}

		SpiffAmount spiffAmount=
				new SpiffAmountImpl(
						spiffEntitlement
						.getSpiffAmount()
					);

		SellDate sellDate=
				new SellDateImpl(
						spiffEntitlement
						.getSellDate()
				);

		return

				new SpiffEntitlementViewImpl(
								spiffEntitlementId,
								accountId,
								partnerSaleRegistrationId,
								facilityName,
								invoiceNumber,
								invoiceUrl,
								partnerRepUserId,
								installDate,
								spiffAmount,
								sellDate
			     );
	}

}
