package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;



@Singleton
class ListClaimSpiffWithIdsFeatureImpl implements
        ListClaimSpiffWithIdsFeature {

    private final SessionFactory sessionFactory;
    private final ClaimSpiffResponseFactory claimSpiffResponseFactory;

    @Inject
    public ListClaimSpiffWithIdsFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final ClaimSpiffResponseFactory claimSpiffResponseFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                        sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.claimSpiffResponseFactory =
                guardThat(
                        "claimSpiffResponseFactory",
                        claimSpiffResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();
    	

    }

    @SuppressWarnings("unchecked")
    @Override
	public Collection<ClaimSpiffView> getClaimSpiffWithIds(
			@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds
			) {
		 Session session = null;
	        try {
	        	
	        	List<Long> claimIds = listClaimSpiffsIds.stream()
	        											.map(claimSpiffId -> claimSpiffId.getValue())
	        											.collect(Collectors.toList());
	        	
	            session = sessionFactory.openSession();

	            List<com.precorconnect.claimspiffservice.database.ClaimSpiff> claimSpiffViews =
	                    session
	                    		.createQuery("from ClaimSpiff id where id.claimId in (:listClaimSpiffsIds)")
	                    		.setParameterList("listClaimSpiffsIds", claimIds)
	                    		.list();
	                    		

	            return claimSpiffViews
	                    .stream()
	                    .map(claimSpiffResponseFactory::construct)
	                    .collect(Collectors.toList());

	        } finally {

	            if (session != null) {
	                session.close();
	            }

	        }
	}

    
}
