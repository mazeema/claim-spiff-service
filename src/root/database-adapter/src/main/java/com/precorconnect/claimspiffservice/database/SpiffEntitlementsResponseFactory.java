package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;

public interface SpiffEntitlementsResponseFactory {

	SpiffEntitlementView construct(
			@NonNull SpiffEntitlement  spiffEntitlement
			);

}
