package com.precorconnect.claimspiffservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;


interface ListClaimSpiffWithIdsFeature {

    public Collection<ClaimSpiffView> getClaimSpiffWithIds(
    		@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds
    		);

}
