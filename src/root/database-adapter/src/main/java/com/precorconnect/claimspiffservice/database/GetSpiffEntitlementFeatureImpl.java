package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public class GetSpiffEntitlementFeatureImpl 
				implements GetSpiffEntitlementFeature {

	private final SessionFactory sessionFactory;
    private final SpiffLogResponseFactory spiffLogResponseFactory;

    @Inject
    public GetSpiffEntitlementFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final SpiffLogResponseFactory spiffLogResponseFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                        sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffLogResponseFactory =
                guardThat(
                        "spiffLogResponseFactory",
                        spiffLogResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();


    }

    @SuppressWarnings("unchecked")
    @Override
	public Collection<SpiffLogView> getEntitlementWithRegistrationId(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId
			){

		Session  session = sessionFactory.openSession();
        try {

        	Query query = session.createQuery("from ClaimSpiff where partnerSaleRegistrationId = :id");

        	query.setParameter("id", partnerSaleRegistrationId.getValue());

        	List<ClaimSpiff> spiffLogs = query.list();

            return spiffLogs
                    .stream()
                    .map(spiffLogResponseFactory::construct)
                    .collect(Collectors.toList());

        } catch(final Exception e) {

        	throw new RuntimeException("exception while getting entitlement records:", e);

        } finally {

            if (session != null) {
                session.close();
            }

        }
	}

}
