package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;

@Singleton
public class UpdateSpiffLogFeatureImpl implements
		UpdateSpiffLogFeature {

    private final SessionFactory sessionFactory;

    private final SpiffLogRequestFactory spiffLogRequestFactory;

    @Inject
    public UpdateSpiffLogFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final SpiffLogRequestFactory spiffLogRequestFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                        sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffLogRequestFactory =
                guardThat(
                        "spiffLogRequestFactory",
                        spiffLogRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public void updateSpiffLog(
			@NonNull SpiffLogDto spiffLogDto
			) {

		Session  session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {

        	ClaimSpiff entityObj =
        				spiffLogRequestFactory.construct(spiffLogDto);

            session.saveOrUpdate(entityObj);

            tx.commit();

        } catch(final Exception e) {

        	tx.rollback();
        	throw new RuntimeException("exception while committing transaction:", e);

        } finally {

            if (session != null) {
                session.close();
            }

        }

	}

}
