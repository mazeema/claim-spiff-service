package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

@Singleton
public class ListSpiffLogsFeatureImpl implements ListSpiffLogsFeature {

    private final SessionFactory sessionFactory;

    private final SpiffLogResponseFactory spiffLogResponseFactory;

    @Inject
    public ListSpiffLogsFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final SpiffLogResponseFactory spiffLogResponseFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffLogResponseFactory =
                guardThat(
                        "spiffLogResponseFactory",
                        spiffLogResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();


    }

	@Override
	public Collection<SpiffLogView> listSpiffLogs() {

		Session  session = sessionFactory.openSession();
        try {

            @SuppressWarnings("unchecked")
			List<ClaimSpiff> spiffLogs =
                    session
                    	.createQuery("from ClaimSpiff")
                    	.list();

            return spiffLogs
                    .stream()
                    .map(spiffLogResponseFactory::construct)
                    .collect(Collectors.toList());

        } catch(final Exception e) {

        	throw new RuntimeException("exception while getting entitlement records:", e);

        } finally {

            if (session != null) {
                session.close();
            }

        }

	}

}
