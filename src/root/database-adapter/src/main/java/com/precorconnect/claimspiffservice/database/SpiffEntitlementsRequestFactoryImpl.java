package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;


@Singleton
public class SpiffEntitlementsRequestFactoryImpl implements SpiffEntitlementsRequestFactory {

	@Override
	public SpiffEntitlement construct(
			@NonNull SpiffEntitlementDto spiffEntitlementDto
			) {

        guardThat(
                  "spiffEntitlementDto",
                   spiffEntitlementDto
                )
                 .isNotNull();

		SpiffEntitlement entityObject = new SpiffEntitlement();

		entityObject.setAccountId(
				spiffEntitlementDto
				.getAccountId()
				.getValue()
				);

		entityObject.setPartnerSaleRegistrationId(
				spiffEntitlementDto
				.getPartnerSaleRegistrationId()
				.getValue()
				);

		entityObject.setFacilityName(
				spiffEntitlementDto
				.getFacilityName()
				.getValue()
				);

		entityObject.setInvoiceNumber(
				spiffEntitlementDto
				.getInvoiceNumber()
				.getValue()
				);

		entityObject.setInvoiceUrl(
				spiffEntitlementDto
				.getInvoiceUrl()!=null?spiffEntitlementDto.getInvoiceUrl().getValue():null);

		entityObject.setPartnerRepUserId(
				spiffEntitlementDto
				.getPartnerRepUserId()!=null?spiffEntitlementDto.getPartnerRepUserId().getValue():null);
		
		if(spiffEntitlementDto.getInstallDate()!=null) {
			entityObject.setInstallDate(
				spiffEntitlementDto
				.getInstallDate()
				.getValue()
				);
		}
		entityObject.setSpiffAmount(
				spiffEntitlementDto
				.getSpiffAmount()
				.getValue());

		entityObject.setSellDate(
				spiffEntitlementDto
				.getSellDate()
				.getValue());

		return entityObject;
	}

}

