package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;


@Singleton
public class ClaimSpiffRequestFactoryImpl implements ClaimSpiffRequestFactory{




	@Override
	public ClaimSpiff construct(
			@NonNull ClaimSpiffDto claimSpiffDto) {

		 guardThat(
                 "claimSpiffDto",
                 claimSpiffDto
               )
                .isNotNull();


		ClaimSpiff entityObject = new ClaimSpiff();

		entityObject
			.setPartnerSaleRegistrationId(
					claimSpiffDto
						.getPartnerSaleRegistrationId()
						.getValue()
							);

		entityObject
		.setPartnerAccountId(
				claimSpiffDto
					.getPartnerAccountId()
					.getValue()
						);

		entityObject
		.setPartnerRepUserId(
				claimSpiffDto
					.getPartnerRepUserId()
					.getValue()
						);
		if(claimSpiffDto
					.getInstallDate().getValue()!=null) {
		entityObject
		.setInstallDate(
				claimSpiffDto
					.getInstallDate().
					getValue()
				);
		}
		entityObject
		.setSpiffAmount(
				claimSpiffDto
					.getSpiffAmount()
					.getValue()
						);

		entityObject
		.setSpiffClaimedDate(
				claimSpiffDto
						.getSpiffClaimedDate()
						.getValue()
								);


		entityObject
			.setFacilityName(
					claimSpiffDto
						.getFacilityName()
						.getValue()
							);

		entityObject
			.setInvoiceNumber(
					claimSpiffDto
						.getInvoiceNumber()
						.getValue()
							);

		entityObject
			.setSellDate(
				claimSpiffDto
					.getSellDate()
					.getValue()
				);

		entityObject
			.setReviewStatus(
					"Un-Reviewed"
					);

		entityObject
			.setReviewedBy(
						null
						);

		entityObject
			.setReviewedDate(
							null
							);

        return
        		entityObject;
	}

}
