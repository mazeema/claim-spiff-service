package com.precorconnect.claimspiffservice.database;

import java.util.Collection;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;

public interface AddClaimSpiffFeature {

	public Collection<ClaimSpiffId> addClaimSpiffs(
    		Collection<ClaimSpiffDto> listClaimSpiffs
    	    );

}
