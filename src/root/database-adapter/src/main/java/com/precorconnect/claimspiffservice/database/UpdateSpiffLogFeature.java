package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;

public interface UpdateSpiffLogFeature {

	public void updateSpiffLog(
							@NonNull SpiffLogDto spiffLogDto
							);
}
