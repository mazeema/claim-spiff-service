package com.precorconnect.claimspiffservice.database;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.flywaydb.core.Flyway;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.core.DatabaseAdapter;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public class DatabaseAdapterImpl implements
        DatabaseAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public DatabaseAdapterImpl(
            @NonNull DatabaseAdapterConfig config
    ) {

        // ensure database schema up to date
        Flyway flyway = new Flyway();
        flyway.setDataSource(
                config.getUri().toString(),
                config.getUsername().getValue(),
                config.getPassword().getValue());
        flyway.migrate();


        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );

        injector =
                Guice.createInjector(guiceModule);

    }

    @Override
    public Collection<ClaimSpiffId> addClaimSpiffs(
    		Collection<ClaimSpiffDto> listClaimSpiffs
    	    ) {

               return  injector
                        .getInstance(AddClaimSpiffFeature.class)
                        .addClaimSpiffs(
                        		listClaimSpiffs
                        		);

    }

   	@Override
	public Collection<ClaimSpiffView> getClaimSpiffsWithIds(
			Collection<ClaimSpiffId> listClaimSpiffsIds
			) {
		return  injector
                .getInstance(ListClaimSpiffWithIdsFeature.class)
                .getClaimSpiffWithIds(
                		listClaimSpiffsIds
                		);
	}

	@Override
	public Collection<SpiffEntitlementId> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements) {

		return
                injector
                        .getInstance(CreateSpiffEntitlementFeature.class)
                        .createSpiffEntitlements(spiffEntitlements);
	}

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId) {

		return
                injector
                        .getInstance(ListSpiffEntitlementFeature.class)
                        .listEntitlementsWithPartnerId(accountId);
	}
	
	@Override
	public Collection<SpiffLogView> getEntitlementWithPartnerSaleRegistrationId(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId) {

		return
                injector
                        .getInstance(GetSpiffEntitlementFeature.class)
                        .getEntitlementWithRegistrationId(partnerSaleRegistrationId);
	}

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl) {

		injector
        		.getInstance(UpdateInvoiceUrlFeature.class)
        		.updateInvoiceUrl(partnerSaleRegistrationId, invoiceUrl);

	}

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserId) {
		injector
				.getInstance(UpdatePartnerRepFeature.class)
				.updatePartnerRep(partnerSaleRegistrationId, partnerRepUserId);

	}

	@Override
	public Collection<SpiffLogView> listSpiffLogs() {

		return
				injector
					.getInstance(ListSpiffLogsFeature.class)
					.listSpiffLogs();
	}

	@Override
	public Collection<SpiffLogView> listSpiffLogsWithId(
			@NonNull AccountId accountId
			) {

		return
				injector
					.getInstance(ListSpiffLogsWithIdFeature.class)
					.listSpiffLogsWithId(accountId);

	}

	@Override
	public void updateSpiffLog(
			@NonNull SpiffLogDto spiffLogDto
			) {

				injector
					.getInstance(UpdateSpiffLogFeature.class)
					.updateSpiffLog(
							spiffLogDto
							);

	}

}
