package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

class GuiceModule
        extends AbstractModule {

    /*
    fields
     */
    private final DatabaseAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull final DatabaseAdapterConfig config
    ) {

        this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindFactories();

        bindFeatures();

    }

    private void bindFactories() {



        bind(ClaimSpiffRequestFactory.class)
        		.to(ClaimSpiffRequestFactoryImpl.class);

        bind(ClaimSpiffResponseFactory.class)
				.to(ClaimSpiffResponseFactoryImpl.class);

        bind(SpiffEntitlementsResponseFactory.class)
        		.to(SpiffEntitlementsResponseFactoryImpl.class);

        bind(SpiffEntitlementsRequestFactory.class)
				.to(SpiffEntitlementsRequestFactoryImpl.class);

        bind(SpiffLogResponseFactory.class)
				.to(SpiffLogResponseFactoryImpl.class);

        bind(SpiffLogRequestFactory.class)
				.to(SpiffLogRequestFactoryImpl.class);

    }

    private void bindFeatures() {

        bind(ListClaimSpiffWithIdsFeature.class)
				.to(InstrumentedListClaimSpiffWithIdsFeatureImpl.class);

        bind(ListClaimSpiffWithIdsFeature.class)
				.annotatedWith(UnderlyingFeature.class)
				.to(ListClaimSpiffWithIdsFeatureImpl.class);

        bind(AddClaimSpiffFeature.class)
				.to(InstrumentedAddClaimSpiffFeatureImpl.class);

        bind(AddClaimSpiffFeature.class)
				.annotatedWith(UnderlyingFeature.class)
				.to(AddClaimSpiffFeatureImpl.class);

        bind(ListSpiffEntitlementFeature.class)
        		.to(ListSpiffEntitlementFeatureImpl.class);

        bind(GetSpiffEntitlementFeature.class)
        		.to(GetSpiffEntitlementFeatureImpl.class);

        bind(CreateSpiffEntitlementFeature.class)
				.to(CreateSpiffEntitlementFeatureImpl.class);

        bind(UpdateInvoiceUrlFeature.class)
				.to(UpdateInvoiceUrlFeatureImpl.class);

        bind(UpdatePartnerRepFeature.class)
				.to(UpdatePartnerRepFeatureImpl.class);

        bind(ListSpiffLogsFeature.class)
				.to(ListSpiffLogsFeatureImpl.class);

        bind(ListSpiffLogsWithIdFeature.class)
				.to(ListSpiffLogsWithIdFeatureImpl.class);

        bind(UpdateSpiffLogFeature.class)
				.to(UpdateSpiffLogFeatureImpl.class);

    }

    @Provides
    @Singleton
    SessionFactory sessionFactory() {

        Configuration configuration = new Configuration();
        configuration.addPackage(getClass().getPackage().getName());
        configuration.addAnnotatedClass(ClaimSpiff.class);
        configuration.addAnnotatedClass(SpiffEntitlement.class);
        configuration.configure();

        configuration.setProperty("hibernate.connection.url", config.getUri().toString());
        configuration.setProperty("hibernate.connection.username", config.getUsername().getValue());
        configuration.setProperty("hibernate.connection.password", config.getPassword().getValue());

        StandardServiceRegistryBuilder builder =
                new StandardServiceRegistryBuilder()
                        .applySettings(
                                configuration.getProperties());

        return configuration
                .buildSessionFactory(
                        builder.build());
    }
}
