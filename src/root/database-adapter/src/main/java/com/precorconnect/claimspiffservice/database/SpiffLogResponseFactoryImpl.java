package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDate;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ReviewStatus;
import com.precorconnect.claimspiffservice.objectmodel.ReviewStatusImpl;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedBy;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedByImpl;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedDate;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogViewImpl;

@Singleton
public class SpiffLogResponseFactoryImpl implements SpiffLogResponseFactory {

	@Override
	public SpiffLogView construct(
			@NonNull ClaimSpiff spiffLog
			) {

		guardThat(
                "spiffLog",
                spiffLog
              )
               .isNotNull();


		ClaimSpiffId claimId = new ClaimSpiffIdImpl(
				spiffLog
				.getClaimId()
				.longValue()
				);

		PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(
				spiffLog
					.getPartnerSaleRegistrationId()
				);

		 AccountId partnerAccountId = new AccountIdImpl(
				 spiffLog
				 .getPartnerAccountId()
				 );

		 UserId partnerRepUserId = new UserIdImpl(
				 spiffLog
				 .getPartnerRepUserId()
				 );

		 SellDate sellDate =  new SellDateImpl(
				 spiffLog
				 .getSellDate()
				 );

		 InstallDate installDate =  new InstallDateImpl(
				 spiffLog
				 .getInstallDate()
				 );

		 SpiffAmount spiffAmount = new SpiffAmountImpl(
				 spiffLog
				 .getSpiffAmount()
				 );

		 SpiffClaimedDate spiffClaimedDate = new SpiffClaimedDateImpl(
				 spiffLog
				 .getSpiffClaimedDate()
				 );

		 FacilityName facilityName = new FacilityNameImpl(
				 spiffLog
				 .getFacilityName()
				 );

		 InvoiceNumber invoiceNumber = new InvoiceNumberImpl(
				 spiffLog
				 .getInvoiceNumber()
				 );

		 ReviewStatus reviewStatus = new ReviewStatusImpl(
				 spiffLog
				 .getReviewStatus()
				 );

		 ReviewedBy reviewedBy = new ReviewedByImpl(
				 spiffLog
				 .getReviewedBy()
				 );

		 ReviewedDate reviewedDate = null;

		 if(spiffLog.getReviewedDate() != null){

		 reviewedDate = new ReviewedDateImpl(
				 						spiffLog
				 						.getReviewedDate()
				 						);

		 }


		return
				new SpiffLogViewImpl(
									claimId,
									partnerSaleRegistrationId,
									partnerAccountId,
									partnerRepUserId,
									sellDate,
									installDate,
									spiffAmount,
									spiffClaimedDate,
									facilityName,
									invoiceNumber,
									reviewStatus,
									reviewedBy,
									reviewedDate
									);
	}

}
