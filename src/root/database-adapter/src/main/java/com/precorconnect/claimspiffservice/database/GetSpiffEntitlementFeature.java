package com.precorconnect.claimspiffservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public interface GetSpiffEntitlementFeature {

	public Collection<SpiffLogView> getEntitlementWithRegistrationId(
			@NonNull PartnerSaleRegistrationId partnerSaleRegId
			);

}
