package com.precorconnect.claimspiffservice.database;

import java.net.URI;
import java.net.URISyntaxException;

import com.precorconnect.Password;
import com.precorconnect.PasswordImpl;
import com.precorconnect.Username;
import com.precorconnect.UsernameImpl;

public class DatabaseAdapterConfigFactoryImpl
        implements DatabaseAdapterConfigFactory {

	@Override
	public DatabaseAdapterConfig construct() {

		return
				new DatabaseAdapterConfigImpl(
									constructURI(),
				                    constructUserName(),
				                    constructPassword()
		);
	}

	public URI constructURI() {

		try {

			String baseURI = System.getenv("DATABASE_URI");

			return
					new URI(
					  baseURI
					);

		} catch (URISyntaxException e) {

			throw new RuntimeException(e);

		}
	}

	public Username constructUserName(){

		return
				new UsernameImpl(
					   System.getenv("DATABASE_USERNAME")
				);

	}

	public Password constructPassword(){

		return
				new PasswordImpl(
					   System.getenv("DATABASE_PASSWORD")
				);

	}
}
