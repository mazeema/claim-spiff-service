ALTER TABLE submittedclaimspiff DROP COLUMN facilityId;

ALTER TABLE submittedclaimspiff MODIFY partnerAccountId VARCHAR(30);

ALTER TABLE submittedclaimspiff MODIFY partnerRepUserId VARCHAR(30);