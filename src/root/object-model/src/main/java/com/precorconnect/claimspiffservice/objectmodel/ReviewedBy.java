package com.precorconnect.claimspiffservice.objectmodel;

public interface ReviewedBy {

	String getValue();

}
