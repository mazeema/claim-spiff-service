package com.precorconnect.claimspiffservice.objectmodel;

public interface PartnerSaleRegistrationId {
	
	Long getValue();
	
}
