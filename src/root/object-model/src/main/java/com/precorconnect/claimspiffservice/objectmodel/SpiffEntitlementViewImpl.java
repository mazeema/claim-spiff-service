package com.precorconnect.claimspiffservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;

public class SpiffEntitlementViewImpl
		implements SpiffEntitlementView {

	/*
	 fields
	*/
   private final SpiffEntitlementId spiffEntitlementId;

   private final AccountId accountId;

   private final PartnerSaleRegistrationId partnerSaleRegistrationId;

   private final FacilityName facilityName;

   private final InvoiceNumber invoiceNumber;

   private final InvoiceUrl invoiceUrl;

   private final UserId partnerRepUserId;

   private final InstallDate installDate;

   private final SpiffAmount spiffAmount;

   private final SellDate sellDate;


   /*
   constructors
   */
   public SpiffEntitlementViewImpl(
		   @NonNull final SpiffEntitlementId spiffEntitlementId,
           @NonNull final AccountId accountId,
           @NonNull final PartnerSaleRegistrationId partnerSaleRegistrationId,
           @NonNull final FacilityName facilityName,
           @NonNull final InvoiceNumber invoiceNumber,
           final InvoiceUrl invoiceUrl,
           final UserId partnerRepUserId,
           @Nullable final InstallDate installDate,
           @NonNull final SpiffAmount spiffAmount,
           @NonNull final SellDate sellDate
   ) {

	   this.spiffEntitlementId =
               guardThat(
                       "spiffEntitlementId",
                       spiffEntitlementId
               )
                       .isNotNull()
                       .thenGetValue();

   	this.accountId =
               guardThat(
                       "accountId",
                       accountId
               )
                       .isNotNull()
                       .thenGetValue();

   	this.partnerSaleRegistrationId =
               guardThat(
                       "partnerSaleRegistrationId",
                       partnerSaleRegistrationId
               )
                       .isNotNull()
                       .thenGetValue();

   	this.facilityName =
               guardThat(
                       "facilityName",
                       facilityName
               )
                       .isNotNull()
                       .thenGetValue();

   	this.invoiceNumber =
               guardThat(
                       "invoiceNumber",
                       invoiceNumber
               )
                       .isNotNull()
                       .thenGetValue();

   	this.invoiceUrl = invoiceUrl;

   	this.partnerRepUserId = partnerRepUserId;

   	this.installDate = installDate;

   	this.spiffAmount =
               guardThat(
                       "spiffAmount",
                       spiffAmount
               )
                       .isNotNull()
                       .thenGetValue();

   	this.sellDate =
            guardThat(
                    "sellDate",
                    sellDate
            )
                    .isNotNull()
                    .thenGetValue();

   }

   /*
   getter & setter methods
   */
	@Override
	public SpiffEntitlementId getSpiffEntitlementId() {

		return spiffEntitlementId;
	}

	@Override
	public AccountId getAccountId() {

		return accountId;
	}

	@Override
	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {

		return partnerSaleRegistrationId;
	}

	@Override
	public FacilityName getFacilityName() {

		return facilityName;
	}

	@Override
	public InvoiceNumber getInvoiceNumber() {

		return invoiceNumber;
	}

	@Override
	public InvoiceUrl getInvoiceUrl() {

		return invoiceUrl;
	}

	@Override
	public UserId getPartnerRepUserId() {

		return partnerRepUserId;
	}

	@Override
	public InstallDate getInstallDate() {

		return installDate;
	}

	@Override
	public SpiffAmount getSpiffAmount() {

		return spiffAmount;
	}

	@Override
	public SellDate getSellDate() {

		return sellDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accountId == null) ? 0 : accountId.hashCode());
		result = prime * result
				+ ((facilityName == null) ? 0 : facilityName.hashCode());
		result = prime * result
				+ ((installDate == null) ? 0 : installDate.hashCode());
		result = prime * result
				+ ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result
				+ ((invoiceUrl == null) ? 0 : invoiceUrl.hashCode());
		result = prime
				* result
				+ ((partnerRepUserId == null) ? 0 : partnerRepUserId.hashCode());
		result = prime
				* result
				+ ((partnerSaleRegistrationId == null) ? 0
						: partnerSaleRegistrationId.hashCode());
		result = prime * result
				+ ((sellDate == null) ? 0 : sellDate.hashCode());
		result = prime * result
				+ ((spiffAmount == null) ? 0 : spiffAmount.hashCode());
		result = prime
				* result
				+ ((spiffEntitlementId == null) ? 0 : spiffEntitlementId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SpiffEntitlementViewImpl other = (SpiffEntitlementViewImpl) obj;
		if (accountId == null) {
			if (other.accountId != null) {
				return false;
			}
		} else if (!accountId.equals(other.accountId)) {
			return false;
		}
		if (facilityName == null) {
			if (other.facilityName != null) {
				return false;
			}
		} else if (!facilityName.equals(other.facilityName)) {
			return false;
		}
		if (installDate == null) {
			if (other.installDate != null) {
				return false;
			}
		} else if (!installDate.equals(other.installDate)) {
			return false;
		}
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null) {
				return false;
			}
		} else if (!invoiceNumber.equals(other.invoiceNumber)) {
			return false;
		}
		if (invoiceUrl == null) {
			if (other.invoiceUrl != null) {
				return false;
			}
		} else if (!invoiceUrl.equals(other.invoiceUrl)) {
			return false;
		}
		if (partnerRepUserId == null) {
			if (other.partnerRepUserId != null) {
				return false;
			}
		} else if (!partnerRepUserId.equals(other.partnerRepUserId)) {
			return false;
		}
		if (partnerSaleRegistrationId == null) {
			if (other.partnerSaleRegistrationId != null) {
				return false;
			}
		} else if (!partnerSaleRegistrationId
				.equals(other.partnerSaleRegistrationId)) {
			return false;
		}
		if (sellDate == null) {
			if (other.sellDate != null) {
				return false;
			}
		} else if (!sellDate.equals(other.sellDate)) {
			return false;
		}
		if (spiffAmount == null) {
			if (other.spiffAmount != null) {
				return false;
			}
		} else if (!spiffAmount.equals(other.spiffAmount)) {
			return false;
		}
		if (spiffEntitlementId == null) {
			if (other.spiffEntitlementId != null) {
				return false;
			}
		} else if (!spiffEntitlementId.equals(other.spiffEntitlementId)) {
			return false;
		}
		return true;
	}


}
