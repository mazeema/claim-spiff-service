package com.precorconnect.claimspiffservice.objectmodel;

public interface SpiffAmount {

	Double getValue();
	
}
