package com.precorconnect.claimspiffservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;

public class SpiffLogViewImpl implements SpiffLogView {

	/*
    fields
     */

	private final ClaimSpiffId claimId;

    private final PartnerSaleRegistrationId partnerSaleRegistrationId;

    private final AccountId partnerAccountId;

    private final UserId partnerRepUserId;

    private final SellDate sellDate;

    private final InstallDate installDate;

    private final SpiffAmount spiffAmount;

    private final SpiffClaimedDate spiffClaimedDate;

    private final FacilityName facilityName;

    private final InvoiceNumber invoiceNumber;

    private final ReviewStatus reviewStatus;

    private ReviewedBy reviewedBy;

    private ReviewedDate reviewedDate;

    public SpiffLogViewImpl(
			@NonNull ClaimSpiffId claimId,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull AccountId partnerAccountId,
			@NonNull UserId partnerRepUserId,
			@NonNull SellDate sellDate,
			@NonNull InstallDate installDate,
			@NonNull SpiffAmount spiffAmount,
			@NonNull SpiffClaimedDate spiffClaimedDate,
			@NonNull FacilityName facilityName,
			@NonNull InvoiceNumber invoiceNumber,
			@NonNull ReviewStatus reviewStatus,
			@NonNull final ReviewedBy reviewedBy,
	        @NonNull final ReviewedDate reviewedDate
			) {

		this.claimId =
                guardThat(
                        "claimId",
                         claimId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                         partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                         partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                         partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

		this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.installDate = installDate;


		this.spiffAmount =
                guardThat(
                        "spiffAmount",
                         spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffClaimedDate =
                guardThat(
                        "spiffClaimedDate",
                         spiffClaimedDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.facilityName =
                guardThat(
                        "facilityName",
                         facilityName
                )
                        .isNotNull()
                        .thenGetValue();


		this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                         invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

		this.reviewStatus = reviewStatus;

    	this.reviewedBy = reviewedBy;

    	this.reviewedDate = reviewedDate;

	}

	public ClaimSpiffId getClaimId() {
		return claimId;
	}

	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public AccountId getPartnerAccountId() {
		return partnerAccountId;
	}

	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public SellDate getSellDate(){
		return sellDate;
	}

	public InstallDate getInstallDate() {
		return installDate;
	}

	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public FacilityName getFacilityName() {
		return facilityName;
	}

	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	public ReviewStatus getReviewStatus(){
		return reviewStatus;
	}

	public ReviewedBy getReviewedBy() {
		return reviewedBy;
	}

	public ReviewedDate getReviewedDate() {
		return reviewedDate;
	}


}
