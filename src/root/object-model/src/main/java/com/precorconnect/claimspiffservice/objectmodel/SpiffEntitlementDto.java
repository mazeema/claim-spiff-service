
package com.precorconnect.claimspiffservice.objectmodel;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;

public interface SpiffEntitlementDto {

    AccountId getAccountId();

    PartnerSaleRegistrationId getPartnerSaleRegistrationId();

    FacilityName getFacilityName();

    InvoiceNumber getInvoiceNumber();

    InvoiceUrl getInvoiceUrl();

    UserId getPartnerRepUserId();

    InstallDate getInstallDate();

    SpiffAmount getSpiffAmount();

    SellDate getSellDate();

}
