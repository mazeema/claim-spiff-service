package com.precorconnect.claimspiffservice.objectmodel;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;

public interface SpiffLogView {
	/*
    fields
     */

	  ClaimSpiffId getClaimId();

      PartnerSaleRegistrationId getPartnerSaleRegistrationId();

      AccountId getPartnerAccountId();

      UserId getPartnerRepUserId();

      SellDate getSellDate();

      InstallDate getInstallDate();

      SpiffAmount getSpiffAmount();

      SpiffClaimedDate getSpiffClaimedDate();

      FacilityName getFacilityName();

      InvoiceNumber getInvoiceNumber();

      ReviewStatus getReviewStatus();

      ReviewedBy getReviewedBy();

  	  ReviewedDate getReviewedDate();
}
