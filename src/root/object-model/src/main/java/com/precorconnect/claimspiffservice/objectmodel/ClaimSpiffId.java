package com.precorconnect.claimspiffservice.objectmodel;

public interface ClaimSpiffId {
	Long getValue();
}
