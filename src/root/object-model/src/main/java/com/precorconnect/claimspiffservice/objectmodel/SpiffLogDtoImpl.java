package com.precorconnect.claimspiffservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;

public class SpiffLogDtoImpl implements SpiffLogDto {

    /*
    fields
    */
	private ClaimSpiffId claimSpiffId;

    private PartnerSaleRegistrationId partnerSaleRegistrationId;

    private final AccountId partnerAccountId;

    private final UserId partnerRepUserId;

    private SellDate sellDate;

    private InstallDate installDate;

    private SpiffAmount spiffAmount;

    private SpiffClaimedDate spiffClaimedDate;

    private FacilityName facilityName;

    private InvoiceNumber invoiceNumber;

    private ReviewStatus reviewStatus;

    private ReviewedBy reviewedBy;

    private ReviewedDate reviewedDate;

    /*
    constructors
    */
    public SpiffLogDtoImpl(
    		@NonNull final ClaimSpiffId claimSpiffId,
            @NonNull final PartnerSaleRegistrationId partnerSaleRegistrationId,
            @NonNull final AccountId partnerAccountId,
            @NonNull final UserId partnerRepUserId,
            @NonNull final SellDate sellDate,
            @NonNull final InstallDate installDate,
            @NonNull final SpiffAmount spiffAmount,
            @Nullable final SpiffClaimedDate spiffClaimedDate,
            @NonNull final FacilityName facilityName,
            @NonNull final InvoiceNumber invoiceNumber,
            @NonNull final ReviewStatus reviewStatus,
            @NonNull final ReviewedBy reviewedBy,
            @NonNull final ReviewedDate reviewedDate
    ) {

    	this.claimSpiffId =
                guardThat(
                        "claimSpiffId",
                        claimSpiffId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                         partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                        partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                         partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

    	this.installDate =
                guardThat(
                        "installDate",
                         installDate
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffAmount =
                guardThat(
                        "spiffAmount",
                         spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffClaimedDate = spiffClaimedDate;


    	this.facilityName=
                guardThat(
                        "facilityName",
                         facilityName
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                         invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

    	this.reviewStatus = reviewStatus;

    	this.reviewedBy = reviewedBy;

    	this.reviewedDate = reviewedDate;

    }

    /*
    getter & setter methods
    */
	public ClaimSpiffId getClaimSpiffId() {
		return claimSpiffId;
	}

	public void setClaimSpiffId(ClaimSpiffId claimSpiffId) {
		this.claimSpiffId = claimSpiffId;
	}

	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public void setPartnerSaleRegistrationId(
			PartnerSaleRegistrationId partnerSaleRegistrationId) {
		this.partnerSaleRegistrationId = partnerSaleRegistrationId;
	}

	public SellDate getSellDate() {
		return sellDate;
	}

	public void setSellDate(SellDate sellDate) {
		this.sellDate = sellDate;
	}

	public InstallDate getInstallDate() {
		return installDate;
	}

	public void setInstallDate(InstallDate installDate) {
		this.installDate = installDate;
	}

	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	public void setSpiffAmount(SpiffAmount spiffAmount) {
		this.spiffAmount = spiffAmount;
	}

	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public void setSpiffClaimedDate(SpiffClaimedDate spiffClaimedDate) {
		this.spiffClaimedDate = spiffClaimedDate;
	}

	public FacilityName getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(FacilityName facilityName) {
		this.facilityName = facilityName;
	}

	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(InvoiceNumber invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public ReviewStatus getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(ReviewStatus reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public AccountId getPartnerAccountId() {
		return partnerAccountId;
	}

	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public ReviewedBy getReviewedBy() {
		return reviewedBy;
	}

	public void setReviewedBy(ReviewedBy reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	public ReviewedDate getReviewedDate() {
		return reviewedDate;
	}

	public void setReviewedDate(ReviewedDate reviewedDate) {
		this.reviewedDate = reviewedDate;
	}


}
