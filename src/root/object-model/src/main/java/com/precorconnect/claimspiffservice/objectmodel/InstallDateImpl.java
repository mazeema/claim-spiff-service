package com.precorconnect.claimspiffservice.objectmodel;

import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;

public class InstallDateImpl implements InstallDate{

    /*
    fields
     */
    private final Date value;
    
    /*
    constructor methods
     */
    public InstallDateImpl(
    		@NonNull Date value
    		){
    	
    	this.value = value;                

    }
    
    /*
    getter methods
     */
	@Override
	public Date getValue() {

		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InstallDateImpl other = (InstallDateImpl) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}


	
}
