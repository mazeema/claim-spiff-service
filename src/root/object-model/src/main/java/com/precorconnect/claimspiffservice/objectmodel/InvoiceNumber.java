package com.precorconnect.claimspiffservice.objectmodel;

public interface InvoiceNumber {

	String getValue();
}
