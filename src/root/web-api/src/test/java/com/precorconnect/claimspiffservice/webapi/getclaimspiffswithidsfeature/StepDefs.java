package com.precorconnect.claimspiffservice.webapi.getclaimspiffswithidsfeature;

import static com.jayway.restassured.RestAssured.given;

import java.util.Collection;
import java.util.StringJoiner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.claimspiffservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.claimspiffservice.webapi.Config;
import com.precorconnect.claimspiffservice.webapi.ConfigFactory;
import com.precorconnect.claimspiffservice.webapi.Dummy;
import com.precorconnect.claimspiffservice.webapi.Factory;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs
		extends AbstractSpringIntegrationTest{

    /*
    fields
     */
    private final Config config =
            new ConfigFactory().construct();

    private final Dummy dummy = new Dummy();

    private final Factory factory =
            new Factory(
                    dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                            config.getIdentityServiceJwtSigningKey()
                    )
            );


    private OAuth2AccessToken accessToken;

    private Response responseOfPostToSpiffEntitlements;

    private Collection<Long> claimSpiffIds;

    @Before
    public void beforeAll() {

        RestAssured.port = getPort();

    }

    /*
    steps
     */
	@Given("^a claimSpiffId list consists of:$")
	public void aclaimSpiffIdlistconsistsof(
			DataTable arg
			)throws Throwable{
		// no op, inputs are taken from dummy
	}

   @Given("^I provide an accessToken identifying me as a partner rep$")
   public void provideAnAccessTokenIdentifyingMeAsAPartnerRep(
   ) throws Throwable {

       accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidPartnerRepOAuth2AccessToken()
													.getValue()
											);

   }

   @Given("^provide a valid claimSpiffId list$")
   public void provideavalidclaimSpiffIdlist(
   ) throws Throwable {

	   claimSpiffIds = dummy.getClaimSpiffIds();

   }

   @When("^I execute getClaimSpiffsWithIds$")
   public void IExecutegetClaimSpiffsWithIds(
   ) throws Throwable {

	   StringJoiner joiner=new StringJoiner(",");

	   	  for(Long claimSpiffId:claimSpiffIds){
	   		joiner.add(""+claimSpiffId);
	   	  }

   	responseOfPostToSpiffEntitlements =
   			given()
            .contentType(ContentType.JSON)
            .header(
                    "Authorization",
                    String.format(
                            "Bearer %s",
                            accessToken
                                    .getValue()
                    )
            )
            .get("/claim-spiffs/claimids/"
            		+joiner);

   }

   @Then("^the claimedspiffs with the matched claimSpiffId are returned$")
   public void theuseridofthematchedpartnersaleregistrationidaremodified(
   ) throws Throwable {

	   responseOfPostToSpiffEntitlements
									.then()
									.assertThat()
									.statusCode(200);

   }

}
