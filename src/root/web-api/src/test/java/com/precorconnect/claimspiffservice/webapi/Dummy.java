package com.precorconnect.claimspiffservice.webapi;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebDto;

public class Dummy {

    /*
    fields
     */
    private URI uri;

    private Collection<ClaimSpiffWebDto> claimSpiffWebDtoList = new ArrayList<ClaimSpiffWebDto>();

    private Collection<Long> claimSpiffIds;

    private String accountId;

    private SpiffEntitlementWebDto spiffEntitlementWebDto;

    private List<SpiffEntitlementWebDto> spiffEntitlementWebDtos = new ArrayList<SpiffEntitlementWebDto>();

    private OAuth2AccessToken accessToken = null;

    private String newInvoiceUrl ;

    private String newUserId;

    private List<Long> spiffEntitlementIds = new ArrayList<Long>();

    /*
    constructors
     */
    public Dummy() {

        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

      String claimedDate = new Timestamp(System.currentTimeMillis()).toString();
        ClaimSpiffWebDto claimSpiffDto =
        		new ClaimSpiffWebDto(
        				1L,
        				123L,
        				"001K000001H2Km2IAF",
        				"00u5i5aimx8ChKQvB0h7",
        				"11/24/2015",
        				"11/24/2015",
        				120.00,
        				claimedDate,
        				"Precor",
        				"111111"
        				);

        claimSpiffWebDtoList.add(claimSpiffDto);

        claimSpiffIds = new ArrayList<Long>();
        claimSpiffIds.add(1L);
        claimSpiffIds.add(2L);

        accountId = "001K000001H2Km2IAF";

        newInvoiceUrl = "http:/s3.amazonaws.com/newinvoice.png";

        newUserId = "00u5iazj60tFcBPCv0h8";

        spiffEntitlementWebDto = new SpiffEntitlementWebDto(
        													"001K000001H2Km2IAF",
        													131L,
        													"precor",
        													"231123",
        													"https://s3.amazonaws.com/partner-sale-invoice-service-dev.precorconnect.com/0027f5ca-4b84-417e-bd91-3ecc8475924a.png",
        													"00u5iazj60tFcBPCv0h7",
        													"11/21/2000",
        													100.00,
        													"11/21/2000"
        													);

        spiffEntitlementWebDtos.add(spiffEntitlementWebDto);

        spiffEntitlementIds.add(1L);
    }

    /*
    getter methods
     */
    public URI getUri() {
        return uri;
    }

	public Collection<ClaimSpiffWebDto> getClaimSpiffWebDtoList() {
		return claimSpiffWebDtoList;
	}

	public Collection<Long> getClaimSpiffIds() {
		return claimSpiffIds;
	}

	public String getAccountId() {
		return accountId;
	}

	public SpiffEntitlementWebDto getSpiffEntitlementWebDto() {
		return spiffEntitlementWebDto;
	}

	public List<SpiffEntitlementWebDto> getSpiffEntitlementWebDtos() {
		return spiffEntitlementWebDtos;
	}

	public OAuth2AccessToken getAccessToken() {
		return accessToken;
	}

	public String getNewInvoiceUrl() {
		return newInvoiceUrl;
	}

	public String getNewUserId() {
		return newUserId;
	}

	public List<Long> getSpiffEntitlementIds() {
		return spiffEntitlementIds;
	}

}
