package com.precorconnect.claimspiffservice.webapi;

import java.math.BigDecimal;
import java.time.Instant;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastNameImpl;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.SapVendorNumberImpl;
import com.precorconnect.UserIdImpl;
import com.precorconnect.identityservice.AppJwt;
import com.precorconnect.identityservice.AppJwtImpl;
import com.precorconnect.identityservice.EmployeeJwt;
import com.precorconnect.identityservice.EmployeeJwtImpl;
import com.precorconnect.identityservice.HmacKeyImpl;
import com.precorconnect.identityservice.PartnerRepJwt;
import com.precorconnect.identityservice.PartnerRepJwtImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

public class GetAuthorization {

	  private static final Dummy dummy =
	    		 new Dummy();


	public static void main(String[] args){


		PartnerRepJwt partnerRepJwt =
                new PartnerRepJwtImpl(
                        Instant.now().plusSeconds(86400),
                        dummy.getUri(),
                        dummy.getUri(),
                        new FirstNameImpl("venkataramana"),
                        new LastNameImpl("P"),
                        new UserIdImpl("00u612sf8vlKu2wpP0h7"),
                        new AccountIdImpl("001K000001H2Km2IAF"),
                        new SapVendorNumberImpl("0000000000")
                );

		OAuth2AccessToken partnertoken = new IdentityServiceIntegrationTestSdkImpl(
				new HmacKeyImpl("nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ")
        )
		.getPartnerRepOAuth2AccessToken(partnerRepJwt);

		AppJwt appJwt = new AppJwtImpl(
							Instant.now().plusSeconds(86400),
			                dummy.getUri(),
			                dummy.getUri()
							);


		OAuth2AccessToken apptoken = new IdentityServiceIntegrationTestSdkImpl(
											        		new HmacKeyImpl("nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ")
											        )
                									.getAppOAuth2AccessToken(appJwt);
		
		EmployeeJwt employeeJwt =
                new EmployeeJwtImpl(
                        Instant.now().plusSeconds(480),
                        dummy.getUri(),
                        dummy.getUri(),
                        new FirstNameImpl("venkataramana"),
                        new LastNameImpl("P"),
                        new UserIdImpl("00u5h8rhbtzPEtCH20h7")
                );
		
		OAuth2AccessToken empToken = new IdentityServiceIntegrationTestSdkImpl(
				new HmacKeyImpl("nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ")
        ).getEmployeeOAuth2AccessToken(employeeJwt);

		String AppAuthorization = String.format(
					                "Bearer %s",
					                apptoken.getValue()
										);

		String PartnerAuthorization = String.format(
                "Bearer %s",
                partnertoken.getValue()
					);
		
		String employeeAuthorization = String.format(
                "Bearer %s",
                empToken.getValue()
					);
		
		
		System.out.println("AppAuthorization : "+AppAuthorization);


		System.out.println("PartnerAuthorization : "+PartnerAuthorization);
		
		System.out.println("employeeAuthorization : "+employeeAuthorization);
	}
}
