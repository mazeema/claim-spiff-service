package com.precorconnect.claimspiffservice.webapi;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDate;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ReviewStatus;
import com.precorconnect.claimspiffservice.objectmodel.ReviewStatusImpl;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedBy;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedByImpl;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedDate;
import com.precorconnect.claimspiffservice.objectmodel.ReviewedDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDtoImpl;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffLogWebDto;

@Component
public class SpiffLogWebRequestFactoryImpl implements SpiffLogWebRequestFactory {

	@Override
	public SpiffLogDto construct(SpiffLogWebDto spiffLogWebDto) {

		ClaimSpiffId claimId=
				new ClaimSpiffIdImpl(
								spiffLogWebDto
								.getClaimId()
								);

		PartnerSaleRegistrationId partnerSaleRegistrationId=
								new PartnerSaleRegistrationIdImpl(
										spiffLogWebDto
										.getPartnerSaleRegistrationId()
										);

		AccountId partnerAccountId=
						new AccountIdImpl(
									spiffLogWebDto
									.getPartnerAccountId()
									);

		UserId partnerRepUserId =
					new UserIdImpl(
								spiffLogWebDto
								.getPartnerRepUserId()
								);


		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date_install;
		try {
			date_install = formatter.parse(spiffLogWebDto.getInstallDate());
		} catch (ParseException e) {
			throw new RuntimeException("Install Date field parse exception: ",e);
		}

		InstallDate installDate = new
									InstallDateImpl(
											date_install
												);

		Date date_sell;
		try {
			date_sell = formatter.parse(spiffLogWebDto
					.getInstallDate());
		} catch (ParseException e) {
			throw new RuntimeException("Sell Date field parse exception: ",e);
		}

		SellDate sellDate = new
								SellDateImpl(
										date_sell
											);

		SpiffAmount spiffAmount= new SpiffAmountImpl(
				 						spiffLogWebDto
				 						.getSpiffAmount()
										);

		DateFormat timeFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	    Date date;
		try {
			date = timeFormatter.parse(spiffLogWebDto.getSpiffClaimedDate());
		} catch (ParseException e) {
			throw new RuntimeException("claimed Date field parse exception: ",e);
		}
	      java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());
		SpiffClaimedDate spiffClaimedDate = new SpiffClaimedDateImpl(
															timeStampDate
															);

		FacilityName facilityName= new FacilityNameImpl(
				 								spiffLogWebDto
				 								.getFacilityName()
												);

		InvoiceNumber invoiceNumber= new InvoiceNumberImpl(
				 								spiffLogWebDto
				 								.getInvoiceNumber()
				 								);

		ReviewStatus reviewStatus= new ReviewStatusImpl(
					 								spiffLogWebDto
					 								.getReviewStatus()
					 								);

		ReviewedBy	 reviewedBy = new ReviewedByImpl(
									 			""
												);


		java.util.Date currentDate = new java.util.Date();

		ReviewedDate reviewedDate = new ReviewedDateImpl(
					 									currentDate
						 								);

		return
				new SpiffLogDtoImpl(
								claimId,
								partnerSaleRegistrationId,
								partnerAccountId,
								partnerRepUserId,
								sellDate,
								installDate,
								spiffAmount,
								spiffClaimedDate,
								facilityName,
								invoiceNumber,
								reviewStatus,
								reviewedBy,
								reviewedDate
								);
	}

}
