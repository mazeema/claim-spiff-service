package com.precorconnect.claimspiffservice.webapi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDate;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDtoImpl;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebDto;


@Component
public class SpiffEntitlementsWebRequestFactoryImpl implements
		SpiffEntitlementsWebRequestFactory {

	@Override
	public SpiffEntitlementDto construct(
				@NonNull SpiffEntitlementWebDto spiffEntitlementWebDto) {

		AccountId accountId = new
								AccountIdImpl(
											spiffEntitlementWebDto
												.getAccountId()
											);


		PartnerSaleRegistrationId partnerSaleRegistrationId = new
																PartnerSaleRegistrationIdImpl(
																		spiffEntitlementWebDto
																			.getPartnerSaleRegistrationId()
																	);

		FacilityName facilityName = new
										FacilityNameImpl(
												spiffEntitlementWebDto
													.getFacilityName()
												);

		InvoiceNumber invoiceNumber = new
										InvoiceNumberImpl(
        										spiffEntitlementWebDto
        											.getInvoiceNumber()
        											);


		InvoiceUrl invoiceUrl = new
									InvoiceUrlImpl(
											spiffEntitlementWebDto
        										.getInvoiceUrl()
                        						);

		UserId partnerRepUserId = spiffEntitlementWebDto.getPartnerRepUserId() != null ? new UserIdImpl(
				spiffEntitlementWebDto.getPartnerRepUserId().trim()) : null;


		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date_install = null;
		try {
			if(spiffEntitlementWebDto.getInstallDate().isPresent()) {
			date_install = formatter.parse(spiffEntitlementWebDto
					.getInstallDate().get());
			}
		} catch (ParseException e) {
			throw new RuntimeException("Install Date field parse exception: ",e);
		}

		InstallDate installDate = new
									InstallDateImpl(
											date_install
												);

		SpiffAmount spiffAmount = new
									SpiffAmountImpl(
											spiffEntitlementWebDto
												.getSpiffAmount()
											);

		Date date_sell;
		try {
			date_sell = formatter.parse(spiffEntitlementWebDto
					.getSellDate());
		} catch (ParseException e) {
			throw new RuntimeException("Sell Date field parse exception: ",e);
		}

		SellDate sellDate = new
								SellDateImpl(
										date_sell
											);

        return
                new SpiffEntitlementDtoImpl(
                		accountId,
                		partnerSaleRegistrationId,
                		facilityName,
                		invoiceNumber,
                		invoiceUrl,
                		partnerRepUserId,
                		installDate,
                		spiffAmount,
                		sellDate
                );
	}

}
