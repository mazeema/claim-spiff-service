package com.precorconnect.claimspiffservice.webapi;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;

@ControllerAdvice
class ExceptionHandler extends ResponseEntityExceptionHandler{

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @org.springframework.web.bind.annotation.
            ExceptionHandler(value = AuthenticationException.class)
    public void authenticationException() {
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @org.springframework.web.bind.annotation.
            ExceptionHandler(value = AuthorizationException.class)
    public void authorizationException() {
    }

    

}
