package com.precorconnect.claimspiffservice.webapi;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDtoImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDate;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementIdImpl;

@Component
public class ClaimSpiffWebRequestFactoryImpl implements ClaimSpiffWebRequestFactory{

	@Override
	public ClaimSpiffDto construct(
			com.precorconnect.claimspiffservice.webapiobjectmodel.@NonNull ClaimSpiffWebDto claimSpiffView) {

		SpiffEntitlementId spiffEntitlementId =
        		new SpiffEntitlementIdImpl(
        				claimSpiffView
        				.getSpiffEntitlementId()
        				);

        PartnerSaleRegistrationId partnerSaleRegistrationId =
        		new PartnerSaleRegistrationIdImpl(
        				claimSpiffView
        				.getPartnerSaleRegistrationId()
        				);

        AccountId partnerAccountId=
        		new AccountIdImpl(
        				claimSpiffView
        				.getPartnerAccountId()
        				);

        UserId partnerRepUserId =
        		new UserIdImpl(
        				claimSpiffView
        				.getPartnerRepUserId()
        				);

        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date date_sell;
		try {
			date_sell = formatter.parse(
						claimSpiffView
						.getSellDate()
					);
		} catch (ParseException e) {
			throw new RuntimeException("Date field parse exception: ",e);
		}

		SellDate sellDate = new
								SellDateImpl(
											date_sell
												);

		Date date_install = null;
		try {
			if(claimSpiffView
						.getInstallDate().isPresent()) {
			date_install = formatter.parse(
						claimSpiffView
						.getInstallDate().get()
					);
			}
		} catch (ParseException e) {
			throw new RuntimeException("Date field parse exception: ",e);
		}

		InstallDate installDate = new
									InstallDateImpl(
											date_install
												);

        SpiffAmount spiffAmount =
        		new SpiffAmountImpl(
        				claimSpiffView
        				.getSpiffAmount()
                        );


        java.util.Date currentDate = new java.util.Date();

		 SpiffClaimedDate spiffClaimedDate=
        		new SpiffClaimedDateImpl(
        				new Timestamp(currentDate.getTime())
        				);

        FacilityName facilityName =
        		new FacilityNameImpl(
        				claimSpiffView
        				.getFacilityName()
        				);

        InvoiceNumber invoiceNumber =
        		new InvoiceNumberImpl(
        				claimSpiffView
        				.getInvoiceNumber()
        				);


        return
                new ClaimSpiffDtoImpl(
                		spiffEntitlementId,
                		partnerSaleRegistrationId,
                		partnerAccountId,
                		partnerRepUserId,
                		sellDate,
                		installDate,
                		spiffAmount,
                		spiffClaimedDate,
                		facilityName,
                		invoiceNumber
                	);
	}

}
