package com.precorconnect.claimspiffservice.webapi;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebView;


public interface ClaimSpiffViewResponseFactory {

	ClaimSpiffWebView construct(
			ClaimSpiffView claimSpiffView
			);
}
