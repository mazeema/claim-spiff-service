package com.precorconnect.claimspiffservice.webapi;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffLogWebView;

public interface SpiffLogWebResponseFactory {

	public SpiffLogWebView construct(
								SpiffLogView spiffLogView
								);
}
