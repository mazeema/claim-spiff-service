package com.precorconnect.claimspiffservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.api.ClaimSpiffService;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffLogWebDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffLogWebView;

@RestController
@RequestMapping("/spiff-logs")
@Api(value = "/spiff-logs", description = "Operations on spiff logs")
public class SpiffLogResource {

    /*
    fields
     */
    private final ClaimSpiffService claimSpiffService;

    private final OAuth2AccessTokenFactory oAuth2AccessTokenFactory;

    private final SpiffLogWebResponseFactory spiffLogWebResponseFactory;

    private final SpiffLogWebRequestFactory spiffLogWebRequestFactory;

    @Inject
    public SpiffLogResource(
            @NonNull final ClaimSpiffService claimSpiffService,
            @NonNull final OAuth2AccessTokenFactory oAuth2AccessTokenFactory,
            @NonNull  final SpiffLogWebResponseFactory spiffLogWebResponseFactory,
            @NonNull  final SpiffLogWebRequestFactory spiffLogWebRequestFactory
    ) {

    	this.claimSpiffService =
                guardThat(
                        "claimSpiffService",
                         claimSpiffService
                )
                        .isNotNull()
                        .thenGetValue();

    	this.oAuth2AccessTokenFactory =
                guardThat(
                        "oAuth2AccessTokenFactory",
                         oAuth2AccessTokenFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffLogWebResponseFactory =
                guardThat(
                        "spiffLogWebResponseFactory",
                        spiffLogWebResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffLogWebRequestFactory =
                guardThat(
                        "spiffLogWebRequestFactory",
                        spiffLogWebRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @RequestMapping(
    		method = RequestMethod.GET
    		)
    @ApiOperation(
    		value = "getting list of spiff logs"
    		)
    public Collection<SpiffLogWebView> listSpiffLogs(
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException{

    	OAuth2AccessToken accessToken = oAuth2AccessTokenFactory
                        					.construct(authorizationHeader);


    	return
    			claimSpiffService
   						.listSpiffLogs(accessToken)
   						.stream()
                        .map(spiffLogWebResponseFactory::construct)
                        .collect(Collectors.toList());

    }

    @RequestMapping(
    		method = RequestMethod.GET,
    		value = "/{accountId}"
    		)
    @ApiOperation(
    		value = "getting list of spiff logs by accountId"
    		)
    public Collection<SpiffLogWebView> listSpiffLogsWithId(
    		@PathVariable("accountId") String accountId,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException{

    	OAuth2AccessToken accessToken = oAuth2AccessTokenFactory
                        					.construct(authorizationHeader);

    	return
    			claimSpiffService
   						.listSpiffLogsWithId(new AccountIdImpl(accountId),accessToken)
   						.stream()
                        .map(spiffLogWebResponseFactory::construct)
                        .collect(Collectors.toList());

    }

    @RequestMapping(
    		method = RequestMethod.POST,
    		value = "/update"
    		)
    @ApiOperation(
    		value = "updating the review status of spiff log"
    		)
    public void updateSpiffLog(
    		@RequestBody SpiffLogWebDto spiffLogWebDto,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException{

    	OAuth2AccessToken accessToken = oAuth2AccessTokenFactory
                        					.construct(authorizationHeader);

    			claimSpiffService
   						.updateSpiffLog(
   								spiffLogWebRequestFactory.construct(spiffLogWebDto),
   								accessToken
   								);

    }

}
