package com.precorconnect.claimspiffservice.webapi;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffLogWebDto;

public interface SpiffLogWebRequestFactory {

	public SpiffLogDto construct(
			SpiffLogWebDto spiffLogWebDto
			);

}
