package com.precorconnect.claimspiffservice.webapi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.stereotype.Component;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffLogWebView;

@Component
public class SpiffLogWebResponseFactoryImpl implements
		SpiffLogWebResponseFactory {

	@Override
	public SpiffLogWebView construct(
			SpiffLogView spiffLogView
			) {

		Long claimId=
				spiffLogView
				.getClaimId()
				.getValue();

		Long partnerSaleRegistrationId=
				spiffLogView
					.getPartnerSaleRegistrationId()
					.getValue();

		String partnerAccountId=
				spiffLogView
				 .getPartnerAccountId()
				 .getValue();

		String partnerRepUserId =
				spiffLogView
				 .getPartnerRepUserId()
				 .getValue();

		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		 String sellDateString = 
				 dateFormat.format(
						 spiffLogView
						 .getSellDate()
						 .getValue()
						 );
												
		 String installDateString = null;

		 if(spiffLogView.getInstallDate().getValue() != null){

		 installDateString = 
				 dateFormat.format(
						 spiffLogView
						 .getInstallDate()
						 .getValue()
				 		);
		 }

		 Double spiffAmount=
				 spiffLogView
				 .getSpiffAmount()
				 .getValue();

		 String claimedDateString = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
		 									.format(spiffLogView.getSpiffClaimedDate().getValue());


		 String facilityName=
				 spiffLogView
				 .getFacilityName()
				 .getValue();

		 String invoiceNumber=
				 spiffLogView
				 .getInvoiceNumber()
				 .getValue();

		 String reviewStatus = null;

		 if(spiffLogView.getReviewStatus() != null){

			 reviewStatus=
					 spiffLogView
					 .getReviewStatus()
					 .getValue();
		 }

		 String reviewedBy = null;

		 if(spiffLogView.getReviewedBy() != null){

			 reviewedBy =
				 spiffLogView
				 .getReviewedBy()
				 .getValue();

		 }

		 String reviewedDate = null;

		 if(spiffLogView.getReviewedDate() != null){

			 reviewedDate =
				 dateFormat.format(
						 	spiffLogView
						 	.getReviewedDate()
						 	.getValue()
						 	);
		 }

		return
				new SpiffLogWebView(
								claimId,
								partnerSaleRegistrationId,
								partnerAccountId,
								partnerRepUserId,
								sellDateString,
								installDateString,
								spiffAmount,
								claimedDateString,
								facilityName,
								invoiceNumber,
								reviewStatus,
								reviewedBy,
								reviewedDate
								);

	}

}
