package com.precorconnect.claimspiffservice.webapi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebView;

@Component
public class SpiffEntitlementsWebResponseFactoryImpl
			implements SpiffEntitlementsWebResponseFactory {

	@Override
	public SpiffEntitlementWebView construct(
			@NonNull SpiffEntitlementView spiffEntitlementView) {

		Long spiffEntitlementId =
				spiffEntitlementView
                        .getSpiffEntitlementId()
                        .getValue();

		String accountId =
				spiffEntitlementView
                        .getAccountId()
                        .getValue();

		Long partnerSaleRegistrationId =
					spiffEntitlementView
						.getPartnerSaleRegistrationId()
							.getValue();

		String facilityName =
						spiffEntitlementView
							.getFacilityName()
							.getValue();

		String invoiceNumber =
						spiffEntitlementView
							.getInvoiceNumber()
							.getValue();

		String invoiceUrl =
						spiffEntitlementView
							.getInvoiceUrl()
							.getValue();

		String partnerRepUserId = null;

		if(spiffEntitlementView.getPartnerRepUserId() != null) {
			partnerRepUserId = 	spiffEntitlementView.getPartnerRepUserId().getValue();
		}
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		
		String installDateString = null;
		
		if(spiffEntitlementView
							.getInstallDate()!=null) {
		installDateString = dateFormat.format(spiffEntitlementView
				.getInstallDate()
				.getValue());
		}
		
		Double spiffAmount =
						spiffEntitlementView
							.getSpiffAmount()
							.getValue();

		Date sellDate =
				spiffEntitlementView
					.getSellDate()
					.getValue();


		

		

		String sellDateString = dateFormat.format(sellDate);

		return
				new SpiffEntitlementWebView(
									spiffEntitlementId,
									accountId,
									partnerSaleRegistrationId,
									facilityName,
									invoiceNumber,
									invoiceUrl,
									partnerRepUserId,
									installDateString,
									spiffAmount,
									sellDateString
						);
	}

}
