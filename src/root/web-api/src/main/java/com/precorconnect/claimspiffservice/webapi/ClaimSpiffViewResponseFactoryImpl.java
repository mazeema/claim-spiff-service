package com.precorconnect.claimspiffservice.webapi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.stereotype.Component;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebView;

@Component
public class ClaimSpiffViewResponseFactoryImpl implements ClaimSpiffViewResponseFactory {

	@Override
	public ClaimSpiffWebView construct(ClaimSpiffView claimSpiffView) {

		Long claimId=
				claimSpiffView
				.getClaimId().getValue();

		Long partnerSaleRegistrationId=
				claimSpiffView
					.getPartnerSaleRegistrationId()
					.getValue();

		String partnerAccountId=
				 claimSpiffView
				 .getPartnerAccountId()
				 .getValue();

		String partnerRepUserId =
				 claimSpiffView
				 .getPartnerRepUserId()
				 .getValue();

		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 String installDateString =  null;
		 if(claimSpiffView.getInstallDate()!=null) {
		 
			 installDateString = dateFormat.format(
				 					claimSpiffView
				 					.getInstallDate()
				 					.getValue());
		 }
		 Double spiffAmount=
				 claimSpiffView
				 .getSpiffAmount()
				 .getValue();


		 String claimedDateString = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
		 								.format(claimSpiffView.getSpiffClaimedDate().getValue());


		 String facilityName=
				 claimSpiffView
				 .getFacilityName()
				 .getValue();

		 String invoiceNumber=
				 claimSpiffView
				 .getInvoiceNumber()
				 .getValue();

		 String sellDateString =
				 dateFormat.format(
				 claimSpiffView
				 .getSellDate()
				 .getValue()
				 );



		return new ClaimSpiffWebView(
				claimId,
				partnerSaleRegistrationId,
				partnerAccountId,
				partnerRepUserId,
				installDateString,
				spiffAmount,
				claimedDateString,
				facilityName,
				invoiceNumber,
				sellDateString
			);
	}

}
