package com.precorconnect.claimspiffservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebView;


public interface SpiffEntitlementsWebResponseFactory {

    SpiffEntitlementWebView construct(
    		  @NonNull SpiffEntitlementView spiffEntitlementView
    );

}
