package com.precorconnect.claimspiffservice.identity;

public interface IdentityServiceAdapterConfigFactory {

	IdentityServiceAdapterConfig construct();
}
