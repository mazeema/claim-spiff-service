package com.precorconnect.claimspiffservice.sdk;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.webapi.Application;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class ClaimSpiffServiceSdkIT {

    /*
    fields
     */
    @Value("${local.server.port}")
    int port;

    private final Dummy dummy =
    		 new Dummy();

    private final Config config =
            new ConfigFactory()
                    .construct();

    private final Factory factory =

    		new Factory(
                    new IdentityServiceIntegrationTestSdkImpl(
                    		config.getIdentityServiceJwtSigningKey()
                    ),
                    dummy
            );


    /*
    test methods
     */
    @Test
    public void addClaimSpiffs_ReturnsOneOrMoreIds(
    ) throws Exception {

        ClaimSpiffServiceSdk objectUnderTest =
                new ClaimSpiffServiceSdkImpl(
                        new ClaimSpiffServiceSdkConfigFactoryImpl()
                        										.construct()
                );

        Collection<ClaimSpiffId>
                actualClaimSpiffClaimIds =
                objectUnderTest.addCliamSpiffs(
                		dummy.getClaimSpiffDtoList(),
                		factory.constructValidPartnerRepOAuth2AccessToken()
                );

        assertThat(actualClaimSpiffClaimIds.size())
                .isGreaterThanOrEqualTo(1);
    }

    @Test
    public void getClaimSpiffsWithIds_ReturnsOneOrMoreClaimSpiffs(
    ) throws Exception {

        ClaimSpiffServiceSdk objectUnderTest =
                new ClaimSpiffServiceSdkImpl(
                        new ClaimSpiffServiceSdkConfigFactoryImpl()
                        										.construct()
                );

        Collection<ClaimSpiffView>
                actualClaimSpiffClaims =
                objectUnderTest.getClaimSpiffsWithIds(
                		dummy.getClaimSpiffIds(),
                		factory.constructValidPartnerRepOAuth2AccessToken()
                );

        assertThat(actualClaimSpiffClaims.size())
                .isGreaterThanOrEqualTo(0);

    }

    @Test
    public void createSpiffEntitlements_ReturnsOneOrMoreIds(
    ) throws Exception {

    	ClaimSpiffServiceSdk objectUnderTest =
                new ClaimSpiffServiceSdkImpl(
                        new ClaimSpiffServiceSdkConfigFactoryImpl()
                        										.construct()
                );

        Collection<Long>
                actualSpiffEntitlementIds =
                objectUnderTest.createSpiffEntitlements(
                		dummy.getSpiffEntitlementDtosList(),
                        factory.constructValidPartnerRepOAuth2AccessToken()
                );

        assertThat(actualSpiffEntitlementIds.size())
                .isGreaterThan(0);

    }

    @Test
    public void listSpiffEntitlements_ReturnsOneOrMoreEntitlements(
    ) throws Exception {

    	ClaimSpiffServiceSdk objectUnderTest =
                new ClaimSpiffServiceSdkImpl(
                        new ClaimSpiffServiceSdkConfigFactoryImpl()
                        										.construct()
                );

        Collection<SpiffEntitlementView>
                actualSpiffEntitlements =
                objectUnderTest.listEntitlementsWithPartnerId(
                		dummy.getSpiffEntitlementDto().getAccountId(),
                        factory.constructValidPartnerRepOAuth2AccessToken()
                );

        assertThat(actualSpiffEntitlements.size())
                .isGreaterThan(0);

    }

    @Test
    public void updateInvoiceUrl_ReturnsNothing(
    ) throws Exception {

    	ClaimSpiffServiceSdk objectUnderTest =
                new ClaimSpiffServiceSdkImpl(
                        new ClaimSpiffServiceSdkConfigFactoryImpl()
                        										.construct()
                );

         objectUnderTest.updateInvoiceUrl(
                		dummy.getSpiffEntitlementDto().getPartnerSaleRegistrationId(),
                		new InvoiceUrlImpl(dummy.getNewInvoiceUrl()),
                        factory.constructValidPartnerRepOAuth2AccessToken()
        		 		);

    }

    @Test
    public void updatePartnerRep_ReturnsNothing(
    ) throws Exception {

    	ClaimSpiffServiceSdk objectUnderTest =
                new ClaimSpiffServiceSdkImpl(
                        new ClaimSpiffServiceSdkConfigFactoryImpl()
                        										.construct()
                );

         objectUnderTest.updatePartnerRep(
                		dummy.getSpiffEntitlementDto().getPartnerSaleRegistrationId(),
                		new UserIdImpl(dummy.getNewUserId()),
                        factory.constructValidPartnerRepOAuth2AccessToken()
        		 		);

    }

}