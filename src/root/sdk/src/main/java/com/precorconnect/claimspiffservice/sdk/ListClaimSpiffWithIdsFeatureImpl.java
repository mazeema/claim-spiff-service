package com.precorconnect.claimspiffservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebView;


@Singleton
class ListClaimSpiffWithIdsFeatureImpl implements
        ListClaimSpiffWithIdsFeature {

    /*
    fields
     */
	private final WebTarget claimSpiffsWebTarget;

	//private final ClaimSpiffWebRequestFactory claimSpiffWebRequestFactory;

    private final ClaimSpiffSdkResponseFactory claimSpiffViewResponseFactory;


    /*
    constructors
     */
    @Inject
    public ListClaimSpiffWithIdsFeatureImpl(
    		 @NonNull final WebTarget baseWebTarget,
             @NonNull final ClaimSpiffSdkResponseFactory claimSpiffViewResponseFactory
    ) {

    	claimSpiffsWebTarget =
                guardThat(
                        "baseWebTarget",
                         baseWebTarget
                )
                        .isNotNull()
                        .thenGetValue()
                        .path("claim-spiffs");

    	this.claimSpiffViewResponseFactory =
                guardThat(
                        "claimSpiffViewResponseFactory",
                         claimSpiffViewResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();
    }



	@Override
	public Collection<ClaimSpiffView> getClaimSpiffsWithIds(
			@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException{

		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        accessToken.getValue()
                );

		String claimIds = listClaimSpiffsIds.stream()
											.map(claimSpiffId -> String.valueOf(claimSpiffId.getValue()))
											.collect(Collectors.joining(","));

        Collection<ClaimSpiffWebView> claimSpiffViews;
        try {
        	claimSpiffViews =
        			claimSpiffsWebTarget
        					.path("/claimids/"+ String.format(
                                            "%s",
                                            claimIds
                                    ))
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .get(
                                    new GenericType<
                                            Collection<ClaimSpiffWebView>>() {
                                    }
                            );

        } catch (NotAuthorizedException e) {

            throw new AuthenticationException(e);

        }

        return
        		 claimSpiffViews
                .stream()
                .map(claimSpiffViewResponseFactory::construct)
                .collect(Collectors.toList());

	}

}
