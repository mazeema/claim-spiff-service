package com.precorconnect.claimspiffservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebDto;


public interface SpiffEntitlementsSdkRequestFactory {

	public SpiffEntitlementWebDto construct(
				@NonNull SpiffEntitlementDto spiffEntitlementDto
			);

}
