package com.precorconnect.claimspiffservice.sdk;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;


public interface ClaimSpiffServiceSdk {

	Collection<ClaimSpiffId> addCliamSpiffs(
			@NonNull Collection<ClaimSpiffDto> claimSpiffDto,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException;

	Collection<ClaimSpiffView> getClaimSpiffsWithIds(
    		@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds,
    		@NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException;

	 Collection<Long> createSpiffEntitlements(
				@NonNull List<SpiffEntitlementDto> spiffEntitlements,
				@NonNull OAuth2AccessToken accessToken
				) throws AuthenticationException, AuthorizationException;

	 Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
							@NonNull AccountId accountId,
							@NonNull OAuth2AccessToken accessToken
						) throws AuthenticationException, AuthorizationException;

	 void updateInvoiceUrl(
			 @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 @NonNull InvoiceUrl invoiceUrl,
			 @NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException, AuthorizationException;

	 void updatePartnerRep(
			 @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 @NonNull UserId partnerRepUserid,
			 @NonNull OAuth2AccessToken accessToken
			 ) throws AuthenticationException, AuthorizationException;

}
