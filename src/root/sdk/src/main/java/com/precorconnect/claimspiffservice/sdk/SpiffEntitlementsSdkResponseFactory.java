package com.precorconnect.claimspiffservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebView;


public interface SpiffEntitlementsSdkResponseFactory {

	public SpiffEntitlementView construct(
  		  						@NonNull SpiffEntitlementWebView spiffEntitlementView
								);
}
