package com.precorconnect.claimspiffservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;


public final class InstrumentedListClaimSpiffWithIdsFeatureImpl implements ListClaimSpiffWithIdsFeature {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentedListClaimSpiffWithIdsFeatureImpl.class);

	private final ListClaimSpiffWithIdsFeature underlyingFeature;

	@Inject
	public InstrumentedListClaimSpiffWithIdsFeatureImpl(@NonNull @UnderlyingFeature final ListClaimSpiffWithIdsFeature underlyingFeature) {
    	this.underlyingFeature =
                guardThat(
                        "underlyingFeature",
                        underlyingFeature
                )
                        .isNotNull()
                        .thenGetValue();

	}


	@Override
	public Collection<ClaimSpiffView> getClaimSpiffsWithIds(
			@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {
		long startTime = System.currentTimeMillis();

		Collection<ClaimSpiffView> result = underlyingFeature
				.getClaimSpiffsWithIds(listClaimSpiffsIds, accessToken);

		long endTime = System.currentTimeMillis();

		LOGGER.debug(String
				.format("[In sdk getClaimSpiffsWithIds call took %s millis.[start=%s, end=%s]",
						(endTime - startTime), new Date(startTime), new Date(
								endTime)));

		return result;
	}

}
