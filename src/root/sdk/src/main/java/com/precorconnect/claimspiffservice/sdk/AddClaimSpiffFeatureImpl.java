package com.precorconnect.claimspiffservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebDto;

public class AddClaimSpiffFeatureImpl
			implements AddClaimSpiffFeature{

	 /*
    fields
     */
	private final WebTarget claimSpiffsWebTarget;

    private final ClaimSpiffSdkRequestFactory claimSpiffWebRequestFactory;

    /*
    constructors
     */
    @Inject
    public AddClaimSpiffFeatureImpl(
    		 @NonNull final WebTarget baseWebTarget,
    		 @NonNull final ClaimSpiffSdkRequestFactory claimSpiffWebRequestFactory

    ) {

    	claimSpiffsWebTarget =
                guardThat(
                        "baseWebTarget",
                         baseWebTarget
                )
                        .isNotNull()
                        .thenGetValue()
                        .path("claim-spiffs");

    	this.claimSpiffWebRequestFactory =
                guardThat(
                        "claimSpiffWebRequestFactory",
                         claimSpiffWebRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();
    }



	@Override
	public Collection<ClaimSpiffId> addCliamSpiffs(
			@NonNull Collection<ClaimSpiffDto> claimSpiffDto,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException{

		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        accessToken.getValue()
                );

		List<ClaimSpiffWebDto> claimSpiffWebDto = claimSpiffDto
		.stream()
        .map(claimSpiffWebRequestFactory::construct)
        .collect(Collectors.toList());

        Collection<Long> claimSpiffIds = new ArrayList<Long>();
        try {

        	claimSpiffIds.addAll(
        			claimSpiffsWebTarget
        					.path("/")
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .post(
                            		Entity.entity(
                            				claimSpiffWebDto,
                                            MediaType.APPLICATION_JSON_TYPE
                                    ),
                                    new GenericType<
                                            Collection<Long>>() {
                                    }
                            ));

        } catch (NotAuthorizedException e) {

            throw new AuthenticationException(e);

        }

        return claimSpiffIds.stream()
        					.map(id-> new ClaimSpiffIdImpl(id))
        					.collect(Collectors.toList());

	}
}
