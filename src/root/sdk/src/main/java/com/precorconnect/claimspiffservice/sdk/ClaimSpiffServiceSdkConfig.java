package com.precorconnect.claimspiffservice.sdk;

import java.net.URL;

public interface ClaimSpiffServiceSdkConfig {

    URL getPrecorConnectApiBaseUrl();

}
