package com.precorconnect.claimspiffservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;

@Singleton
public class UpdateInvoiceUrlFeatureImpl implements UpdateInvoiceUrlFeature {

    /*
    fields
     */
    private final WebTarget spiffEntitlementsWebTarget;

    @Inject
    public UpdateInvoiceUrlFeatureImpl(
    		@NonNull final WebTarget spiffEntitlementsWebTarget
    ) {

    	this.spiffEntitlementsWebTarget =
                guardThat(
                        "spiffEntitlementsWebTarget",
                        spiffEntitlementsWebTarget
                )
                        .isNotNull()
                        .thenGetValue().path("/claim-spiffs/entitlements");

    }

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException, AuthorizationException {

		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        oAuth2AccessToken.getValue()
                );

		try {

            		spiffEntitlementsWebTarget
            				.path("/"+partnerSaleRegistrationId.getValue())
            				.path("/invoiceurl")
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .post(
                            		Entity.entity(
                            				invoiceUrl.getValue(),
                                            MediaType.APPLICATION_JSON_TYPE
                            				)
                            		);

        } catch (NotAuthorizedException e) {

            throw new AuthenticationException(e);

        }

	}

}
