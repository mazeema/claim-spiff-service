package com.precorconnect.claimspiffservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URL;

import org.checkerframework.checker.nullness.qual.NonNull;

public class ClaimSpiffServiceSdkConfigImpl
        implements ClaimSpiffServiceSdkConfig {

    /*
    fields
     */
    private final URL precorConnectApiBaseUrl;

    /*
    constructors
     */
    public ClaimSpiffServiceSdkConfigImpl(

            @NonNull final URL precorConnectApiBaseUrl
    ) {

    	this.precorConnectApiBaseUrl =
                guardThat(
                        "precorConnectApiBaseUrl",
                        precorConnectApiBaseUrl
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */

    @Override
	public URL getPrecorConnectApiBaseUrl() {
        return precorConnectApiBaseUrl;

    }

    /*
    equality methods
    */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        ClaimSpiffServiceSdkConfigImpl that = (ClaimSpiffServiceSdkConfigImpl) o;

        return precorConnectApiBaseUrl.equals(that.precorConnectApiBaseUrl);

    }

    @Override
    public int hashCode() {
        return precorConnectApiBaseUrl.hashCode();
    }
}
