package com.precorconnect.claimspiffservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;

public class InstrumentedAddClaimSpiffFeatureImpl implements AddClaimSpiffFeature{

	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentedAddClaimSpiffFeatureImpl.class);

	private final AddClaimSpiffFeature underlyingFeature;

	@Inject
	public InstrumentedAddClaimSpiffFeatureImpl(@NonNull @UnderlyingFeature final AddClaimSpiffFeature underlyingFeature) {

		this.underlyingFeature =
                guardThat(
                        "underlyingFeature",
                        underlyingFeature
                )
                        .isNotNull()
                        .thenGetValue();

	}

	@Override
	public Collection<ClaimSpiffId> addCliamSpiffs(
			@NonNull Collection<ClaimSpiffDto> claimSpiffDto,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {

		long startTime = System.currentTimeMillis();

		Collection<ClaimSpiffId> result = underlyingFeature
				.addCliamSpiffs(claimSpiffDto, accessToken);

		long endTime = System.currentTimeMillis();

		LOGGER.debug(String
				.format("[In sdk addCliamSpiffs call took %s millis.[start=%s, end=%s]",
						(endTime - startTime), new Date(startTime), new Date(
								endTime)));

		return result;
	}

}
