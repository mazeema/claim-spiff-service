package com.precorconnect.claimspiffservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;

@Singleton
public class UpdatePartnerRepFeatureImpl implements UpdatePartnerRepFeature {

    /*
    fields
     */
    private final WebTarget spiffEntitlementsWebTarget;

    @Inject
    public UpdatePartnerRepFeatureImpl(
    		@NonNull final WebTarget spiffEntitlementsWebTarget
    ) {

    	this.spiffEntitlementsWebTarget =
                guardThat(
                        "spiffEntitlementsWebTarget",
                        spiffEntitlementsWebTarget
                )
                        .isNotNull()
                        .thenGetValue().path("/claim-spiffs/entitlements");

    }

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserid,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException, AuthorizationException {

		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        oAuth2AccessToken.getValue()
                );

		try {

            		spiffEntitlementsWebTarget
            				.path("/"+partnerSaleRegistrationId.getValue())
            				.path("/partnerrepuserid/"+partnerRepUserid.getValue())
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .post(null);

        } catch (NotAuthorizedException e) {

            throw new AuthenticationException(e);

        }
	}

}
